\chapter{Implementation} \label{chap:implem}
This chapter discusses the details of the implementation of \system on the basis of the design choices of the previous chapter. \cref{sec:impl_approach} shows the way \system is implemented inside the Linux kernel, while \cref{sec:overall} gives an overview of the various components added with \system.\\
\Cref{sec:color_mgmt} explains how \system represents colors and manages them through mcolors; in particular, we present how the knowledge of the hash function for Sandy Bridge allows \system to associate a color to a buddy uniquely, and how the color chasing mechanism is implemented thanks to the information in \cref{chap:sandy_reverse}. \Cref{sec:cacheset_impl} shows how colors are associated to applications via the cgroup interface and \cref{sec:rain_buddy} discusses how \system's buddy allocator uses this information.

\section{Implementation approach} \label{sec:impl_approach}
The design proposed for \system in \autoref{chap:design} is implemented on the Linux kernel version \linver.

This choice is due to two main reasons. The first is the need of making modifications starting from a recent version of the memory allocator: in fact, this subsystem has undergone important changes over the years, and is now very different from earlier versions. To derive a modern implementation for \system, a modern allocator is the only choice. The second reason lies in some of the changes the latest versions bring, related to cgroups. While older versions didn't offer a clear interface to add a cgroup, requiring developers to deal with low-level filesystem management details, from version 3.16 Linux developers introduce a unique, high-level \gls{api} that prevents disruptive interventions and eases development. In version \linver this interface has definitely been stabilized and polished.

Because of the fundamental role of the physical memory allocator, this subsystem cannot be plugged or unplugged at runtime, and must be statically linked inside the Linux kernel binary. Hence, the only way to implement \system in Linux was to add the new source code to the kernel working tree and to switch between the default allocator and \system at compile time.

Particular care is devoted to keeping higher levels of the Linux memory management system unchanged, in order to have the smallest possible impact on the existing codebase and functionalities. Therefore, normal applications should not be isolated and should allocate across all the available pages. Most importantly, it is fundamental to preserve the functionality and performance of the kernel without restricting the memory it can allocate from, also in presence of colored tasks.

\section{Coloring the memory of a task} \label{sec:overall}
Before showing the details of the implementation, it is useful to provide an overall view of how \system works, and of how it integrates within Linux in a non-disruptive way. Therefore, this section shows the main steps required to run isolated applications and how the novel mechanisms of \system act within a running Linux kernel.

When a task starts first, no coloring information is attached to it; if no colored applications are running, the task can allocate everywhere in memory. Otherwise, a global data structure stores the colors not allocated to any set of tasks (recalling that, via the cacheset cgroup, the administrator can create groups of tasks sharing the same \gls{llc} partition), and the task allocates from this pool of pages.

If, at some point, the administrator colors the task by writing its \gls{pid} into a cacheset, it restricts the pages available for future allocations to the cacheset pool. Therefore, the administrator can choose to isolate a task from the very beginning or only from a certain point on, for example when its \gls{llc} usage grows over a threshold or when it starts causing sensible pollution to other applications.

Once colored, any memory request from the task will be fulfilled using the cacheset pool. The only mechanism for userspace tasks to obtain physical pages is by causing a page fault. After this event, the kernel recognizes the faulting application and satisfies the memory request by calling the physical memory allocator. This component searches a suitable page via the buddy algorithm and returns it to the fault handler, which in turn updates the virtual-to-physical mapping of the task's memory. In \system, the memory allocator checks whether the requesting task has an associated color set and eventually uses it to restrict the allocation to the allotted colors. If \system's allocator cannot find any coloring information, it uses the default color pool.


\section{Color management} \label{sec:color_mgmt}

\subsection{Representation of color sets} \label{sec:cset}
During a colored allocation, the allocator should be able to select a color from a set in an efficient way. Therefore, \system needs a suitable representation of a set of colors. \Cref{sec:buddydata_mod} introduce the concept of mcolors to generalize the notion of color to the whole buddy allocator, where the size of managed memory areas varies with the order. Thus, a \gls{llc} reservation enforced through page coloring could be represented at any granularity by associating mcolors to a task rather than colors. However, to allow the most possible fine-grained partitioning capability, we choose to associate base colors to a task.

High level structures exist to represent sets of data, with or without an order, like lists or trees, but they have complex management algorithms and typically require several memory accesses, resulting cache-unfriendly. To devise the right implementation for \system, it is important to note that the information about coloring, once set, is unlikely to change: hence, the pooling facility the cacheset cgroup creates makes it easier to create different hierarchical pools rather and moving applications between pools rather than chaning a single color pool. Furthermore, the number of colors an architecture provides is typically small. Therefore, it is natural to describe a set of colors with a bitmask, for which Linux provides pre-built creation and management facilities.

Together with this information, it is important to store also the number of colors a mask contains, to evaluate the memory it can use, and the last color the task allocated from, in order to distribute allocations on colors in a round-robin manner. \system organizes these data in a structure, as \cref{cod:cinfo} shows.

\begin{lstlisting}[label=cod:cinfo,caption={Colors set representation}]
struct color_info {
	colormask_t cmask;
	unsigned int last_color;
	unsigned int count;
};
\end{lstlisting}

This information must be associated to the task once this is colored. The most direct and efficient way is storing this structure inside the one that contains all the information of a process in memory, that is the \code{task_struct} structure at the base of Linux application management. Therefore, \system adds a field \code{struct color_info *cinfo} for this purpose. During the task creation, this fiels is initialized to the value of the task's father, in order to maintain isolation in particular when a task creates threads (Linux spawns threads via the \code{fork} system call and represents them with dedicated \code{task_struct}s, in a similar way to independent processes). The default value for this field is \code{NULL}, meaning that the task is not colored.

In a similar way to a task's color information, \system defines two global variables to store global coloring information of type \code{struct color_info}. The first one is \code{cinfo_kernel}, which stores all the colors the machine has, is initialized at runtime and is not modified; this is the pool the kernel allocates from. The second variable is \code{cinfo_allowed}, which stores the colors no cacheset still uses: non-colored applications allocate from this pool, which does not intersect with any cacheset pool, thus guaranteeing isolation of colored applications from non-colored ones. This variable is updated whenever a cacheset is created or destroyed.


\subsection{Implementation of mcolors} \label{sec:chandle}
The definition of mcolors given in \cref{sec:buddydata_mod} simplifies the management of the buddy data structure when looking for a specific color, avoiding lookups along lists of buddies. Similarly, in \cref{sec:buddyalg_mod} we introduced the function $ mcolor\_lookup_N(c,d) $, which returns the $d$-order mcolor from a color $c$. These notions are at the heart of \system, and their implementation must be efficient.\\
Linux identifies a buddy through its first page, where the information about the buddy are stored. In turn, Linux identifies a physical page with a so-called \gls{pfn}, which is simply the page physical address without the offset bits, after a right-shift of 12 bits on x86 architectures. This is the starting point from which mcolors are read for any buddy order.

In Nehalem, the implementation of the function $mcolor_N(b,d)$ of \cref{sec:buddydata_mod} follows closely from the mathematical definition: since the number of bits considered for the mcolor increases with the order, a right-sift can discard lower bits based on the order, as in \cref{cod:mcolor_N}.
\begin{lstlisting}[label=cod:mcolor_N,caption={Macro to retrieve the buddy mcolor in Nehalem}]
#define page_mcolor(page, order)									\
	( (((unsigned long)page_to_pfn((page))) & linear_color_bitmask)	\
		>> (cshifts[order]) )
\end{lstlisting}
\code{linear_color_bitmask} is a bitmask that keeps only the colors bits from the \gls{pfn}. The variable \code{cshift} is an array of shift indexed per order and initialized at boot time: for orders 0 to 3, the shift is 3, for order 4 it is 4 and so on, as from the definition of $mcolor_N(b,d)$. The function \code{page_to_pfn}, insted, is provided by the kernel and translates a \code{struct page} variable (that represents a page in Linux ' pages array) to its \gls{pfn}.\\
In a similar way, the function $ mcolor\_lookup_N(c,d) $ in Nehalem is implemented in \cref{cod:mcolor_lookup_N}
\begin{lstlisting}[label=cod:mcolor_lookup_N,caption={Macros to compute the mcolor from a color in Nehalem}]
#define mcolor_from_color(color, order) (vcolor >> (cshifts[order] - cshifts[0]))
\end{lstlisting}

As anticipated in the design, the case for Sandy Bridge is, in general, more complex. Following the definition of page color for Sandy Bridge in \cref{sec:sandy_part}, the implementation can be derived, as visible in \cref{cod:sandy_color}.
\begin{lstlisting}[label=cod:sandy_color,caption={Function to retrieve the page color in Sandy Bridge}]
unsigned int page_color(struct page *p)
{
	unsigned long _pfn = page_to_pfn(p);
	unsigned int hash = sb_hash(_pfn);
	return ((hash << (2)) | (( _pfn & linear_color_bitmask) >> 3));
}
\end{lstlisting}
Here, \code{sb_hash} is the function that computes the hash starting from the page address, as reconstructed in \cref{sec:rec_myhash}, and \code{linear_color_bitmask} keeps only the ``linear part'' of the color bits, that is to say those color bits that are not used for the hash (in \ralphcpunobrand and \mysandynobrand, bits 15 and 16). These bits are right-shifted of 3 positions since bits 12 to 14 are not used as color bits (we recall that \code{_pfn} is already right-shifted by 12, being a \gls{pfn}) and finally concatenated on the left with the hash.\\
The hardest, hash-dependent point with Sandy Bridge is implementing the $mcolor_{SB}$ function; here, the presence of the hash creates issues: at order 6, where Nehalem uses only bit 18 to indicate the mcolor,  it is not possible to choose bit $h_2$ (the highest bit in the hash) for the same purpose, since, even knowing $h_2$, it is impossible to predict the future configuration $h_2 h_1$ on a general basis. Therefore, we have to rely on the knowledge of the hash function for our implementation. The basic insight is that bit 17 is used in both the hashes $h_2$ and $h_1$. If we consider the hash computed after zeroing bit 17, we can find that if bit 17 is 1, the final hash is the bit-flip of the previous one, otherwise is is the same; for example, if the hash with bit 17 equal to 0 is $01_2$ and bit 17 is 1, then the final hash is $10_2$. Then the hint consists in grouping together buddies depending on their hash having equal bits or not. If, for example, the allocator looks for a buddy with hash $10_2$, hence with different bits, it should pick a buddy whose hash without bit 17 has different bits. If, still as an example, the hash without bit 17 is $10_2$, the allocator should split this buddy and select the first half, having bit 17 equal to 0, which does not change the hash. Conversely, if the hash without bit 17 is $01_2$, taking the second half of the buddy, with bit 17 equal to 1, ``flips'' the hash and gives $10_2$. With this rationale, at order 6 the buddies should have mcolor 0 if they have hash, without bit 17, of equal bits ($00_2$ or $1_2$), and mcolor 1 if the bits are different.
Higher orders, instead, have all color 0, while lower orders compute the color by concatenating the hash with one or both color bits 16 and 15.

All this could be implemented in a function that, considering the order passed in input, can select the proper case. However, we believe this to be an inefficient implementation. Therefore, we decided to implement this function as a lookup table indexed by page color and order, as in the following code
\begin{lstlisting}[label=cod:sandy_mcolor_jump,caption={Lookup table to retrieve the mcolor in Sandy Bridge}]
uint8_t color_jumps[NR_COLORS][MAX_ORDER];
\end{lstlisting}
so that the $mcolor_{SB}$ function is implemented as
\begin{lstlisting}[label=cod:sandy_mcolor,caption={Function to retrieve the mcolor in Sandy Bridge}]
unsigned int page_mcolor(struct page *p, unsigned int order)
{
	unsigned int color = page_color(p);
	return color_jumps[color][order];
}
\end{lstlisting}
and the $ mcolor\_lookup_{SB}(c,d)$ as
\begin{lstlisting}[label=cod:sandy_mcolor_lookup,caption={Function to retrieve the mcolor in Sandy Bridge}]
#define mcolor_from_color(color, order) (color_jumps[color][order])
\end{lstlisting}
Either using a function or a lookup table keeps the implementation hash-dependent, since their rationale is based on the particular role of bit 17 within the hash. An idea for a general enough implementation is using a lookup table with values initialized at boot time: once the kernel recognizes the \gls{cmp} it runs on, knowing its hash function, it can provide the logic to fill the lookup table properly. Again, this qould require the cooperation of \gls{cmp} manufacturers to have exact information.


\section{Rainbow Buddy} \label{sec:rain_buddy}
In this section we show the major modifications to the Buddy algorithm, according to the design proposed in \cref{sec:buddy_mod}. \Cref{subsec:impl_struct} shows how the Buddy data structures have been modified, while \cref{subsec:impl_alg} shows the changes of the algorithm.

\subsection{The modified Buddy structure} \label{subsec:impl_struct}
In the Linux kernel, the physical memory of a machine is firstly divided into nodes, to manage the memory of different \gls{numa} nodes. Then, each node is divided into zones. Zones are non overlapping memory areas used for different types of allocation; they are defined at compile time and depend on the architecture of the machine \footnote{For example, in a x86-64 system three zone usually exist: the \emph{DMA} zone consists in the first 16 MB of memory and is meant for old \gls{dma} controllers, which use only 24 bits for addressing; the \emph{DMA32} zone extends from 16 MB to 4 GB and is used for \gls{dma} controllers using 32 bits for the physical address; the \emph{Normal} zone is used to control the rest of the memory.}. Applications are preferably given memory from a zone called ``Normal'', but the allocator falls back to the other areas if no memory is present.

Within Linux, a zone is described by many fields; among them, the field \code{struct free_area free_area[MAX_ORDER]} is the main data structure described in \autoref{fig:buddystruct}, and the constant \code{MAX_ORDER} is the number of buddy orders allowed for the current architecture (10 by default). In particular, the \code{struct free_area} implements the structure of the buddy allocator for a single order, comprising the various per-migratetype lists. In fact, this data structure is natively defined as 
\begin{lstlisting}[label=cod:freearea,caption={The original buddy data structure}]
struct free_area {
 	struct list_head	free_list[MIGRATE_TYPES];
	unsigned long		nr_free;
};
\end{lstlisting}
To split each list into per-color sublists according to \cref{sec:buddydata_mod}, the definition changes into
\begin{lstlisting}[label=cod:buddy_freearea,caption={The new buddy data structure}]
struct free_area {
	struct list_head	free_list[MIGRATE_TYPES][NR_COLORS];
	unsigned int last_color[MIGRATE_TYPES];
	unsigned int count_migtype[MIGRATE_TYPES];
	unsigned long		nr_free;
};
\end{lstlisting}
In the original buddy data structure of \cref{cod:freearea}, the allocator can check if there are available buddies for a certain migratetype by simply checking the list head. Instead, with the modifications of \cref{cod:buddy_freearea}, it would be necessary to check each list. To avoid such operation, we added a counter storing the number of buddies for each migratetype. The allocator can check this counter before going through the lists, but it has to update the count in case of buddy removal or insertion.

In general, the allocations of a physical page happens frequently in a running system, especially if with high load conditions. In Linux, the structure of the buddy allocator has a centralized management, and is protected with a spinlock. Since this mechanism does not scale with \glspl{cmp}, Linux employs per-core pools to fulfill page requests, while for higher-order buddies the allocator goes through the buddy data structure. These pools are refilled at runtime in batch and are meant to fulfill most of the memory requests, as applications are given only single pages after a page fault. The per-zone data structure, called \code{per_cpu_pageset}, that implements this pool is originally defined as 
\begin{lstlisting}[label=cod:pcparea,caption={The per-cpu-pages original structure}]
struct per_cpu_pages {
	int count;		/* number of pages in the list */
	int high;		/* high watermark, emptying needed */
	int batch;		/* chunk size for buddy add/remove */
	/* Lists of pages, one per migrate type stored on the pcp-lists */
	struct list_head lists[MIGRATE_PCPTYPES];
};
\end{lstlisting}
Like for the zone \code{free_area}, \system splits the buddy list and adds counters to store the actual availability of buddies per-migratetype.

\begin{lstlisting}[label=cod:buddy_pcparea,caption={The modified per-cpu-pages structure}]
struct per_cpu_pages {
	int count;		/* number of pages in the list */
	int high;		/* high watermark, emptying needed */
	int batch;		/* chunk size for buddy add/remove */
	struct list_head lists[MIGRATE_PCPTYPES][NR_COLORS];
	unsigned int last_color[MIGRATE_PCPTYPES];
	unsigned int count_migtype[MIGRATE_PCPTYPES];
};
\end{lstlisting}

The \code{free_area} is initialized at boot time by forcibly freeing all the memory pages; in this way, the allocator triggers the coalescing procedure that groups buddies into buddies of order higher and higher. Finally, the allocator initializes the \code{per_cpu_pages} with a batch of pages, according to pre-defined quotas. Even with our modifications the initialization follows the same procedure, but for the fact that buddies are stored in the proper list according to their mcolor.

\subsection{The modified Buddy algorithm} \label{subsec:impl_alg}
In Linux, the process of allocating physical memory is very complex, as it considers the multiple subdivisions of memory areas (nodes, zones, etc.) and the per-cpu and migratetype pools, with heuristics to move pages among pools and fallback lists to move to other zones or nodes in case of memory exhaustion on the local node. Therefore, an allocation can be performed in several, more and more complex, attempts. Overall, the highest entry point for the physical memory allocator if the function \code{__alloc_pages_nodemask}, which determines the migratetype based on the caller kernel subsystem and iterates over the allowed machines nodes. Then, \code{get_page_from_freelist} iterates over the zones, in turn calling \code{buffered_rmqueue}, which function first looks for a page in the per-cpu pagesets, and in case it cannot find one, it goes through the buddy allocator. Hence, \code{buffered_rmqueue} is the initial point for the necessary modifications.

The first change to this function consist in retrieving the information of the reserved colors of the process that faulted. To this aim, we added the following code snippet at the beginning
\begin{lstlisting}[label=cod:aaa,caption={Code to choose the color pool to allocate from}]
if((gfp_flags & (GFP_DMA | GFP_DMA32 | GFP_ATOMIC)) ||
	(order > 0) || !ccount(get_allowed_cinfo())) {
	ci = get_kernel_cinfo();
} else if(!colored_task(current)) {
	ci = get_allowed_cinfo();
} else {
	ci = get_task_cinfo(current);
}
\end{lstlisting}
The first \code{if} statement checks whether the target for the allocation is the kernel (for a \gls{dma} allocation, for example), and eventually selects as color set the \code{cinfo_kernel} pool; \code{ci}, in fact, is a pointer to a \code{struct color_info} and is passed to lower layers of the allocator to provide information about the \code{cinfo_allowed} is used, or the task's pool if available.


Once the color pool is chosen, if a page is requested the allocator attempts to allocate from the per-cpu pageset. To choose an allowed color, \system adds the following code to iterate on the bitmask over the task's allowed colors.
\begin{lstlisting}[label=cod:bbb,caption={Color-aware pageset search}]
color = find_color_round(cmask(ci),get_last_color(ci));
list = &pcp->lists[migratetype][color];
while(list_empty(list)) {
	color = find_color_round(cmask(ci),color + 1);
	list = &pcp->lists[migratetype][color];
}
\end{lstlisting}
Here, \code{find_color_round} iterates over the task's colors by jumping to the initial color if it reaches the last; furthermore, it returns if it has iterated over all the colors. In particular, within \cref{cod:bbb}, the starting color given in input to \code{find_color_round} is color successor to the one used in the last allocation. This information, stored inside each \code{struct color_info} as a hint to the allocator, permits to iterate in a round-robin manner over colors, thus distributing allocations over all colors. In fact, starting from a fixed color would use all the pages of the first color before using others, causing a lot of task's data to be mapped to a single \gls{llc} portion and under-exploiting the cache. Moreover, distributing allocations decreases the probability of emptying one list, in turn decreasing the number of iterations required to find a color with available pages.

If instead the allocation order is greater than 0, the allocator goes through the per-order \code{free_area}s, passing the variable \code{ci} as the color pool.

Lower levels make several attempts to allocate a page from migratetype pools, in case previous attempts fail. In particular, \code{buffered_rmqueue} calls \code{__rmqueue}, which makes a first attempt to allocate a page from the requested migratetype via \code{__rmqueue_smallest}, whose failure forces \code{__rmqueue} to call \code{__rmqueue_fallback}, which iterates through all the migratetypes to find a suitable page. Both \code{__rmqueue_smallest} and \code{__rmqueue_fallback} implement the basic concepts of the buddy algorithm, looking for a buddy of the desired order and eventually splitting a larger one. These functions receive the color pool \code{ci} from \code{buffered_rmqueue}, and similarly search for a color with available buddies starting from the last used color. If no buddy is available, the search moves to a higher order, where it is fundamental to look only among buddies of the suitable mcolor, so that the split procedure is guaranteed to return the desired color. To this aim, these functions employ the macro \code{mcolor_from_color} previously defined to compute the mcolor from the desired color and the current order they are looking in. Once a buddy to split is found, both functions call \code{expand} to perform the splitting. This function splits a buddy in two buddies of lower order, stores one inside the list of free buddies and further splits the other, until it obtains a buddy of the requested order. To finally obtain the desired color, \system changes the splitting procedure according to \cref{sec:buddyalg_mod} in order to implement color chasing. While in the standard implementation the half to split is determined a priori (the first one), \system determines it at runtime, with the following check:
\begin{lstlisting}[label=cod:ccc,caption={Color chasing implementation}]
if(mcolor_from_color(color,high) != mcolor_from_color(__page_color,high) ) {
	struct page *tmp = twin;
	twin = page;
	page = tmp;
	__page_color = page_color(page);
}
list_add(&twin->lru, &area->free_list[migratetype][page_mcolor(twin,high)]);
\end{lstlisting}
where \code{page} is the first half of the split buddy, \code{twin} the second half, \code{color} the desired color, \code{__page_color} the color of the first half and \code{high} the current order. As visible in the \code{if} statement, if the mcolor of the first half is different from the desired mcolor, the variables holding the two halves are swapped, and the undesired half stored in the list of free buddies according to its color.

\section{Implementation of the cacheset cgroup} \label{sec:cacheset_impl}
Using the new facilities of Linux version \linver, the cacheset cgroup integrates into the kernel to provide userspace with a file-like interface. On the kernel side, cacheset hooks into the Linux cgroup subsystem by defining the variable
\begin{lstlisting}[label=cod:ccc,caption={Main data structure for cacheset cgroup}]
struct cgroup_subsys cacheset_cgrp_subsys = {
	.css_alloc	= cacheset_css_alloc,
	.css_free	= cacheset_css_free,
	.attach		= cacheset_attach,
	.can_attach	= cacheset_can_attach,
	.exit = cacheset_exit,
	.bind		= cacheset_bind,
	.legacy_cftypes	= files,
	.early_init	= 1,
};
\end{lstlisting}
which follows a strict name convention to be automatically recognized as a cgroup at compilation time. This structure exports the kernel and userspace interfaces to cacheset, in particular the functions that are called during the lifetime of cacheset. For the userspace interface, the field \code{legacy_cftypes} contains a list of file names and functions: when an application writes a file, the related functions are called for input parsing, for providing information, etc.; in this way, a user can, for example, set the number of desired colors by writing a color sequence to a file, and \system applies this setting to the cgroup.\\
The other fields inside \code{cacheset_cgrp_subsys} constitute the kernel interface. When, for example, a process tries to hook into a cacheset my writing its \gls{pid} into a file, the kernel calls the function \code{can_attach} to perform initial checks, and the function \code{attach} to finally perform the attachment.

Internally, all these functions represent a cacheset with the data structure in \cref{cod:ddd}.
For example, \code{attach} links the color set information to the \code{task_struct} of the inserted process and perform bookkeeping, like incrementing \code{task_count} and \code{thread_count}.
\begin{lstlisting}[label=cod:ddd,caption={The cacheset data structure, to store information of a single cacheset}]
struct cacheset {
	struct cgroup_subsys_state css;
	unsigned int nesting_level;
	atomic_t task_count;
	atomic_t thread_count;
	struct color_info cinfo;
	spinlock_t cinfo_lock;
};
\end{lstlisting}
Among other fields in \cref{cod:ddd}, \code{css} connects other cacheset in a tree-like manner, and \code{cinfo} the set of colors.
