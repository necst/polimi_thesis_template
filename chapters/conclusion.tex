\chapter{Conclusions} \label{chap:conclusion}

\epigraph{
\itshape
As you set out for Ithaka\\
hope the voyage is a long one,\\
full of adventure, full of discovery.\\
Laistrygonians and Cyclops,\\
angry Poseidon - don't be afraid of them:\\
you'll never find things like that on your way\\
as long as you keep your thoughts raised high,\\
as long as a rare excitement\\
stirs your spirit and your body.\\
}{
\textit{Konstantin Kavafis, Ithaka}\\
translated by Edmund Keeley
}


Reviewing the work done within this thesis, this chapter derives the conclusions from what has been presented so far.
Considering the goals behind \system, \cref{sec:contrib}, discusses the contributions of this work and with its limits with respect to current state of the art approaches in the field. Instead, \cref{sec:future} shows possible work and research directions starting from \system.

\section{Contributions} \label{sec:contrib}
Considering the state of the art in \cref{chap:soa}, this work provides several contributions.
A first contribution of this work is the systematic identification of the constraints posed by page coloring when used with modern commodity \glspl{cmp}, as discussed in particular in \cref{sec:consequences}. A second contribution is the implementation of page coloring with recent \gls{cmp} architectures like Sandy Bridge, with a thorough evaluation of all the related aspects. To the best of our knowledge, no prior work addressed the novel challenges of the hash-based addressing scheme these architectures adopt\footnote{The only prior work that, at the date of puslishing, implements page coloring on a Sandy Bridge \gls{cmp} is \cite{coloris}, which, yet, does not claim to control the \llc slice data are mapped to}. Instead, this work shows how page coloring can be implemented even on these architectures, overcoming the difficulties due to the unpredictable mapping.\\
This contribution is particularly important in the context of cloud platforms, a spreading phenomenon nowadays. In fact, also these infrastructure can benefit from page coloring, despite having modern hash-addressed caches.

The third contribution is the adoption of the cgroup interface, which allows a centralized control of the \llc partitions on behalf of a workload manager, a component typical of cloud-like environments. Moreover, this interface allows ease of use and deep control at the same time, and is becoming a widespread interface paradigm in today's Linux installations. From this point of view, \system is ``on track'' with recent facilities for applications control, and provides a suitable interface to experiment policies and allow application characterization.

\section{Limits of the present work} \label{sec:limit_future}
Despite the novel contributions this work brings, several aspects still deserve more investigation and effort.

The first issue to tackle regards \gls{i/o} activity, in particular the pollution caused by \gls{dma} drivers. After the results in \cref{chap:results}, the need to coordinate \gls{i/o} buffers activity with \system's mechanisms is clear in order to achieve the strictest isolation. Looking at the state-of-the-art in \cref{chap:soa}, research work already exists that investigates these aspects \cite{srm_buf}, but it is to be integrated into \system's design, so that the allocation of both the applications' memory and the buffers are automatically managed by the kernel.

To test \system's capabilities in production scenarios, a deeper evaluation is needed. The choice of the \gls{spec} suite, in fact, imposes that all the test applications are single-threaded and \gls{cpu}-intensive, representing only a part of today's workloads running within distributed environments. Therefore, it is important to validate \system with a more diverse set of tests. In particular, cloud-like applications with network \gls{i/o} patterns and multi-threaded implementation are a key scenario.\\
Related to this aspect, it is also important to have a better understanding of how the cache-memory constraint discussed in \cref{sec:consequences_mem} plays with real workloads, whose memory requirement can be unknown or unpredictable when the decision about \llc partitioning is made. In fact, a bad choice can later harm isolation, especially if an application with growing memory requirement is assigned colors that are already in use for other tasks.

More investigation is also required to study the impact of renouncing to hugepages. Even if this feature is rarely used in today's applications (and no \gls{spec} test uses it), the ever increasing amount of data to elaborate will potentially push the usage of hugepages. With this vision, the incompatibility of hugepages and page coloring requires high-level decisions to choose whether to exploit the former or the latter feature, and a deeper study on the pros and cons of them.


\section{Future work} \label{sec:future}
Following the above-mentioned limits of the current work, we stress the importance of testing \system in real-world, cloud-like scenarios, that should comprise also batch workloads (e.g., Hadoop) and latency-sensitive applications (e.g., web search).

To envision the employment of page coloring in real-world environments, the main lack is a dynamic mechanism. This mechanism should monitor at runtime the status of each application, devise a proper \llc partitioning scheme and apply it, possibly resizing partitions and removing colors to an application. To implement this feature while guaranteeing strict isolation, re-coloring is needed. Yet, it has a high cost that is still to be evaluated on latest hardware and is potentially open to new solutions. Furthermore, a dynamic mechanism opens many research possibilities about the policies that should drive re-coloring, and which metrics are to be considered.

A promising scenario for \system, \glspl{vm} are widespread in cloud environments, as they enforce isolation among co-running workloads of different users. To further enhance this isolation also at the level of the \llc, \system would be particularly interesting to test, in particular with latest Sandy Bridge \glspl{cmp}. Moreover, the ever-increasing availability of \llc space inspires complex scenarios in which, for example, a 2-levels \gls{llc} partitioning mechanism is in place: the first level runs in the hypervisor and partitions the \gls{llc} among the \glspl{vm}, while the second level runs inside each \gls{vm} and sub-divides the \gls{llc} among applications. However, such an organization requires a complex orchestration between the hypervisor and guest \glspl{os}, which cannot control the physical mapping and perform page coloring directly.

Another suggestion of this work is the possibility of partitioning the lower layers of caches through the \gls{llc}, which could be a novel idea for \gls{smt} architectures and is currently a completely unexplored area. Due to the sharing of the \gls{l1} cache by the threads running on the same physical core, contention on this layer is likely to occur, and should be evaluated.

Finally, as more \gls{cpu} manufacturers are showing interest in the market of distributed platforms and servers, and some already have market niches, \system could be ported and tested on other architectures like ARM, Sparc and IBM Power. 

\section{Final considerations}
Through page coloring, this work achieved control over the \llc, a fundamental component in today's computing platforms. Through \system, the control over this resource is given to the software, which can manage the \llc according to high-level constraints and objectives.\\
Yet, other resources are still hardly partitionable, or not partitionable at all: \gls{cpu}-to-memory bandwidth, \gls{i/o} bandwidth, etc. Anyway, a centralized coordination of partitioned resources is missing, so that unbalanced situations can happen without the software to be aware of them. Moreover, the ever-increasing demand of computational power and the spreading of heterogeneous computational resources with higher energy efficiency open new dimensions in the spaces of monitoring and of possible control actions. Because of these challenges, the research community advocates both software and hardware changes. One the software side, the research is still looking for general enough solution to schedule a wide set of resources for computation and communication. On the hardware side, manufacturers are unwilling to provide control interfaces to the software layer, mainly because this increases the complexity of the hardware and implies deep design shifts.

Facing this lack of control interfaces, this work has attempted to give an enabling technology to achieve this control over today's \glspl{llc}, fulfilling a request that, in the broad view of this section, opens more and more research opportunities.
