\chapter{Experimental Results} \label{chap:results}
In this chapter we present the results obtained through \system. The final goal is to achieve isolation of co-running applications inside the \gls{llc} using the capabilities \system provides. This isolation must be guaranteed in both the Nehalem and the Sandy Bridge platforms. \Cref{sec:env} explains the experimental environment setup by describing the two platforms used for testing \system, the tests adopted to show \system's effectiveness and the measurement methodology. Following these guidelines, \cref{sec:app_char} shows how the test applications behave with different \gls{llc} reservations, in order to devise the tests' sensitiveness to this resource and provide a reference behavior for co-location. \Cref{sec:coloc_w} analyzes how \system is able to isolate diverse co-running applications on an on-demand basis, hence with an \llc partition of varying size. Similarly, \cref{sec:coloc_w_nehalem} measures the effectiveness of \system with a more regular workload, where polluting \gls{i/o} patterns are more limited. Finally, \cref{sec:results} summarizes the results of this chapter.


\section{Experimental environment} \label{sec:env}
Our evaluation comprises several aspects. The first aspect to be evaluated is \system's effectiveness to partition the \gls{llc}, guaranteeing isolation to requesting applications. The second aspect to be studied is how \gls{llc} partitioning impacts on the target architectures for which \system is designed.
To this aim, it is important to choose the experimental environment and the test cases properly. Therefore, \cref{sec:testbed} provides the details about the environment setup, in which \system will run the applications chosen in \cref{sec:test_apps}. Finally, \cref{sec:tools} explains the measurement details and tools.

\subsection{Testbed and coloring parameters} \label{sec:testbed}
For our test, we employed two different machines, one equipped with a Nehalem \gls{cmp} and the other with a Sandy Bridge \gls{cmp}. These machines are representative of low- and middle- end servers typical of computing environments.
The Nehalem machine has a 64 bit quad-core \mycpu \gls{cmp}, with a clock frequency of 2.93 GHz and 12 GB of \gls{ram}. In particular, \mycpunobrand has three layers of cache, with a fixed line size of 64 B:
\begin{itemize}
\item a shared \gls{l3} (\gls{llc}) cache of 8 MB, with 8192 sets and 16-way associativity
\item a per-core \gls{l2} cache of 256 KB, with 512 sets and 8-way associativity
\item a per-core \gls{l1} instruction cache of 32 KB, with 128 sets and 4-way associativity
\item a per-core \gls{l1} data cache of 32 KB, with 64 sets and 8-way associativity
\end{itemize}
The parameters of the caches are the following:
\begin{itemize}
\item 12 bits of physical page offset
\item 6 bits of cache line offset
\item 13 bits of \gls{llc} set number
\item 9 bits of \gls{l2} set number
\item 7 bits of \gls{l1} set number (considering only the instruction cache, the one with more sets)
\end{itemize}
Since the physical page size in x86 architectures is 4 KB, he fundamental parameters for page coloring are:
\begin{itemize}
\item $ 13 + 6 - 12 = 7 $ bits in common between \gls{llc} set index and page address
\item $ 9 + 6 - 12 = 3 $ bits of \gls{l2} set index that overlap with the previous bits
\item $ 7 - 3 = 4 $ bits of color, thus with 16 possible partitions of 512 KB each; these color bits are bits 15 to 18
\end{itemize}

Similarly, the other platform is a Sandy Bridge \gls{cmp} \mysandy running at 2.8 GHz frequency with 6 GB of \gls{ram}. In this architecture, only the upper \gls{llc} is different from Nehalem's, as it is 20-way associative, still with 8192 sets, and is split in four slices of 2048 sets each, connected through the familiar ring-interconnection. Overall, its capacity is 10 MB. Therefore, the parameters \system leverages for this \gls{cmp} are the following:
\begin{enumerate}
\item $ 11 + 6 - 12 = 5 $ bits in common between per-slice \gls{llc} set index and page address
\item still 3 bits of \gls{l2} set index overlapping with \gls{llc} set index,
\item $ 5 - 3 = 2 $ bits of per-slice color, thus with 16 possible partitions of 512 KB each; these color bits are bits 15 and 16
\item $ 2 $ bits of hash, for a total of $ 4 $ bits for the color
\end{enumerate}

Because of the complexity of modern architectures, several capabilities can affect the measurements in an unpredictable way. Cache prefetchers aggressively load data according to speculative decisions based on the access pattern, and can create pollution or become a lieu of contention \cite{Fedorova:2010:MCS:1646353.1646371}. Hyper-Threading support \cite{ht} causes two threads to share many resources inside a single core (lower caches, physical registers, execution units, ...), unpredictably disturbing each other. Finally, the Turbo Boost capability \cite{turbo} aggressively boosts the core frequency for limited time periods, but is entirely under the control of the hardware according to internal, undisclosed policies. Therefore, we disabled all these sources of noise.

The software environment comprises the Ubuntu Linux distribution, version 12.04 \gls{lts}, running on a \system-modified kernel. As the default setting, the maximum order for buddies is 10, so that the largest allocatable area spans 4 MB.

\subsection{Test applications} \label{sec:test_apps}
To stress \system's capabilities, \gls{cpu}-intensive applications are needed. A common reference in the literature, the \gls{spec} CPU2006 benchmark suite \cite{spec}, offers a wide variety of \gls{cpu}-intensive applications with diverse characteristics and cache access patterns \cite{Cook:2013:HEC:2485922.2485949}. \gls{spec} tests are single-threaded applications, a characteristic that allows us to evaluate the features of \system while co-locating different applications without any disturbance from the kernel scheduler or from external, thread-management libraries. In particular, multi-threaded applications have more complex access patterns towards the shared \gls{llc}, which could hinder the evaluation of \system.

Since many \gls{spec} applications run with more input sets sequentially, they can have different phases that can present different access patterns. Therefore, we chose to separate all the possible inputs of each application and to evaluate each one separately, collecting only the execution time. At the end of the evaluation, we selected 8 benchmarks with different access patterns in order to have a representative set of patterns. For each application we chose the input set causing the longest run. The selected applications and inputs are reported in \cref{tab:spec_apps}.

\input{chapters/spec_apps}

\subsection{Experimental methodology and tools} \label{sec:tools}
To characterize the \gls{llc} pattern of an application and its implications on the overall behavior. Two different metrics are needed. For the pattern, the most suitable metric is the \gls{llc} miss rate, which shows the ability of an application to effectively exploit the \gls{llc}. To characterize the overall behavior of an application, also the runtime is a suitable metric that suggests how the performance change.\\
To collect these metrics, several tools are available; for these work, we chose \code{perf}, \cite{perf} which is integrated in Linux and exploits the hardware performance counters available in modern x86 architectures. In particular, we used the \emph{perf stat} command to choose the events to measure and run the test.

Throughout our experiments, all the applications are forced to run on a fixed core (core 0, unless specified otherwise) through the \code{taskset} command. This avoids overheads due to the kernel moving the application, which causes the application to re-load data into the \gls{l2} and \gls{l1} caches. Moreover, power-controlling mechanisms have been disabled and the fixed maximum frequency is set pn all the cores.

To launch an application and give it a \gls{llc} partition, a small launcher application has been developed, which takes in input the cacheset and the target application to run. This launcher retrieves its own \gls{pid}, writes it into the cacheset proper file and launches the target application with an \emph{exec} call, so that the target application ``inherits'' the \gls{llc} partition from the launcher.


\section{Application characterization} \label{sec:app_char}
To characterize the access pattern, we build an accurate profile of each application's behavior with respect to varying size of caches. The obtained profiles will indicate the sensitivity of each application to the cache space, guiding the choice of suitable workloads to show the effectiveness of \system in a co-location scenario. Each application is run 10 times with a different \gls{llc} partition, on both the Nehalem and the Sandy Bridge machines.
Since \gls{llc}-sensitive applications typically reach an execution plateau when the cache space increases, it is interesting to profile them in particular when the \gls{llc} reservation is small, where the behavior is typically more diverse. In the following measurements, the test application is the only one running inside the machine, and thus its behavior is optimal with respect to a co-location scenario.

\subsection{Profiles in Nehalem}
\begin{figure}
\centering
\includegraphics[height=.9\textheight]{figures/results/nehalem_single.pdf}
\caption{Applications profiles in Nehalem with different cache partitions}
\label{fig:app_char_nehalem}
\end{figure}
\Cref{fig:app_char_nehalem} shows the profiles of the test applications running in our Nehalem machine: the red curve plots the execution slowdown with respect to the case with full cache (8 MB) and its scale is on the left side, while the blue curve plots the miss rate and its reference axis is on the right. Each point in the plots is the average of all the 10 measures. Moreover, for each measure the plots also show the \gls{sem} with bars, which are hardly visible in most cases.\\
As we expected, the \gls{llc} profiles are quite diverse: for example, libquantum is almost insensitive to the \gls{llc} size, while bzip2 and omnetpp benefit from more space. However, the miss rate of two cache-friendly applications as bzip2 and omnetpp can still differ slightly, as the miss rate curves of these two applications show. In general, this is due both to the access pattern and the size of the working set. In the specific case of bzip2 and omnetpp, bzip2 has a larger working set than omnetpp: hence, we can infer that the difference is due only to the access pattern. In fact, while bzip2, a compression application, has very strong locality, omnetpp scans a large part of the dataset to perform an event-drive simulation of a complex ethernet network.

In general, looking at \cref{fig:app_char_nehalem} we can note that the \gls{sem} is small across the measures. This suggests that the selected applications have regular access patterns, hence with a predictable behavior. Secondly, the predictability of the runs suggest that the design and the implementation allow \system to effectively control the \gls{llc}.\\
An exception is sphinx, whose \gls{sem} is clearly visible. Sphinx is a speech recognition system based on Viterbi search \cite{1054010} and on beam search heuristic \cite{Shapiro:1992:EAI:531550}, which is a graph-search algorithm. Furthermore, sphinx loads multiple files from disk to recognize multiple words. Therefore, sphinx has irregular access patterns that depend on input data and on the position of memory areas in each single run. In fact, with more than 6 MB of \gls{llc} its performance and miss rate is more moderate, as most of its working set fits in the \gls{llc}.

\subsection{Profiles in Sandy Bridge} \label{sec:app_char_sandy}
Performing the same measurements in Sandy Bridge, we obtained the plots in \cref{fig:app_char_sandy}. Here, applications run on core 0 and are given colors starting from slice 0 and then reaching the other slices in order. Each slice has a size of 2.5 MB and has increasing latency when accessed from core 0 (as from \cref{tab:sandy_latencies}). 
Here, the limited \gls{ram} memory (6 GB) poses some constraints on the tests: bzip2 and gcc, in fact, are not run with only 1.25 MB of \gls{llc} because of the cache-memory constraint identified in \cref{sec:consequences_mem}. Thus, 1.25 MB over 10 MB corresponds to 12.5\% of the \gls{llc} size, constraining the memory to only 750 MB, while these two applications have a barely superior memory footprint (around 850 MB). Therefore, the profile of bzip2 and gcc start from 1.88 MB of \gls{llc}.

With respect to \cref{fig:app_char_nehalem}, we can see, overall, that the runs are steadily more sensitive to the increasing \gls{llc} partition size. Indeed, if we consider the slowdown with 1.25 MB of \llc, it is in every profile higher than that in 1 MB of \cref{sec:consequences_mem}. This is even more interesting if we consider the higher associativity of Sandy Bridge (20) with respect to Nehalem's (16). A higher associativity should decrease the miss rate with the same amount of \llc, as it clearly happens with regular applications, whose miss rate curves values in Sandy Bridge are slightly lower than the corresponding values in Nehalem. Thanks to the higher associativity, the difference between the worst and best miss rate should be smaller. Instead, this difference is comparable, and it is even higher in the case of libquantum. Unlike with Nehalem, in Sandy Bridge libquantum shows some degree of \llc-sensitiveness. This sensitiveness can be explained with the hash-based addressing scheme. If a cache line can be mapped to a slice only, it can be mapped to a single set, whose associativity is 20. If, instead, it can be mapped to two slices, it can end up in two different sets, perceiving an associativity that is, roughly, the double. Therefore, as the \llc an application can use increases, also the associativity it perceives increases, with a consequent gain in terms of reduced miss rate. Previous work on Intel's caches \cite{stuffed} also found similar phenomena, which are, overall, new and not systematically studied.

\begin{figure}
\centering
\includegraphics[height=.9\textheight]{figures/results/sandy_single.pdf}
\caption{Applications profiles in Sandy Bridge with different cache partitions}
\label{fig:app_char_sandy}
\end{figure}

Finally, we have to note the strange behavior of leslie3d and xalancbmk with 5 MB of \llc, in which the miss rate increases with respect to the 3.75 MB point. Further investigation found that it is due to their working set having internal memory areas that have low access rate: if these areas map to a considerable extent with areas of higher access rate, they cause pollution. Moreover, the round-robin policy for color allocation within \system could, for certain configurations, cause polluting overlaps between memory areas having different locality. In particular, xalancbmk outputs a very large amount of data (around 100 MB), with a cache-unfriendly streaming pattern.


\section{Isolation with mixed workloads} \label{sec:coloc_w}
With the cache profiles collected previously, we can classify applications based on their \llc ``sensitivity'', that is to say on how much applications benefit from receiving more cache space. Therefore, we focus on the slowdown for this classification.

We classify applications as \emph{sensitive} when their slowdown with the least amount of cache is equal or greater than 30\%, while the others are \emph{insensitive}. The value of 30\% derives from the direct observation of the plots in \cref{fig:app_char_nehalem,fig:app_char_sandy}, and allows us to well separate applications in the two distinct groups. This holds for both the architectures, where the same value gives the same classification results. In Sandy Bridge, bzip2 and gcc have not been profiled with 1.25 MB of \llc for the reasons explained above, and thus their classification is not directly available. To estimate their slowdown, we computed the slowdown slope in Nehalem in the range 1.25 - 1.88 MB and used this value to compute the slowdown with Sandy Bridge and 1.25 MB of \llc, resulting with the same classification of Nehalem.
\Cref{tab:spec_cat} shows the classification of the \gls{spec} benchmarks used for the evaluation.

Starting from the classification, we randomly chose four workloads with a sensitive application, called \emph{target}, and three other applications, called \emph{polluters}, that run simultaneously. To have a diverse mix, the first polluter is chosen from the sensitive applications while the other two polluters are insensitive applications. \Cref{tab:spec_wl_w} shows the four workloads, with the target and the polluters.

\input{chapters/spec_categ}

The experiment consists in isolating the target, running on core 0, through \system's capabilities, while the polluters run on the other three cores. In particular, the target is assigned an \llc partition, while the polluters contend for the rest of the cache. Like previous measurements, the size of the target's \llc partition is varied throughout the tests, in order to reconstruct a profile of how each target behaves in co-location with the polluters. Furthermore, if a polluter terminates before the target, it is immediately restarted.\\
It is fundamental to note that, in such experiments, the target can receive only a part of the \llc, while a considerable part is left to the polluters. This is particularly important because of the cache-memory constraint of \cref{sec:consequences_mem}: if the target receives, for example, 80\% of the \llc, then it also receives 80\% of the system memory, and only 20\% of \gls{ram} is left to the three polluters, which could severely conflict for memory pages and cause swapping.
\input{chapters/spec_wl_w}

\subsection{Co-location in Nehalem} \label{sec:coloc_w_nehalem}
\begin{figure}
\includegraphics[width=1.1\textwidth]{figures/results/nehalem_workload_w.pdf}
\caption{Profiles of the workloads in \cref{tab:spec_wl_w} on Nehalem, with different cache partitions}
\label{fig:nehalem_workload_w}
\end{figure}
\Cref{fig:nehalem_workload_w} shows the targets' profiles  obtained in co-location on the \mycpunobrand. Each plot is named after the workload name and the target application. The red continue line shows the slowdown, while the blue continue line shows the miss rate. Instead, the dash-dotted lines show how the target behaves when not partitioned, being free to contend the \llc with the polluters. For the sake of comparison, the plots also show the stand-alone target profiles of \cref{fig:app_char_nehalem} with dashed lines and the same colors. For the unpartitioned execution, the \glspl{sem} for the slowdown and for the miss rate are reported on the right side.

In co-location, it is evident that regular applications benefit more from \llc partitioning: in fact, in W1 and W2 the miss rate soon drops below the dash-dotted line, in a controllable way; the same happens for the slowdown, which is tightly related to the miss rate. In particular, bzip2 experiences strong contention when running un-partitioned, and immediately benefits from \system. In fact, despite it has a large memory footprint, it has a small cache working set thanks to the zip compression algorithm, which seeks symbols subsequences inside small chunks of the entire dataset. Omnetpp has, instead, a greater cache working set, thus needing a larger partition. Unlikely, less regular patterns like W3 and W4 benefit less from isolation, since their miss rate and slowdown do not have a monotonically descending behavior. We will explain these behaviors, in a more general view, in the following paragraphs.

Focusing on the single profiles, we can see very different behaviors with respect to the stand-alone execution. This is visible with W1, W3 and W4. In particular, we can notice that with 5 MB of \llc the miss rate increases with respect to 4 MB. Furthermore, W3 and W4 have miss rate curves that do not follow the stand-alone miss rate curve closely. The only very regular pattern is that of W2, where the miss rate in co-location is very close to the stand-alone profile.

Investigating these irregularities, we can anticipate the results of the following sections to explain the divergent patterns. Through further measurements, we found that the deviations from the stand-alone profiles in \cref{fig:app_char_nehalem} are due to \gls{i/o} activity. In fact, the workloads in \cref{tab:spec_wl_w} contain applications with very diverse execution times and \gls{i/o} phases overlapping with the target's execution. For example, in W1, xalancbmk and leslie3d have execution time longer than the target bzip2, while gcc has shorter execution time and is loaded twice during the execution of bzip2. Furthermore, gcc has a considerable working set (around 350 MB) and sudden spikes at the beginning, to load inputs from disk. The same holds for W3, with xalancbmk lasting much longer than gcc. Similarly, in W4 all the applications last much less than sphinx, and are reloaded multiple times, causing numerous \gls{i/o} bursts; this holds in particular for bzip2, which loads its large working set multiple times. Furthermore, since sphinx has non-optimal access patterns, it receives limited benefits from partitioning if, at the same time, pollution happens; indeed, its performance is better than the unpartitioned case only when a large portion of \llc is reserved, because most of its large working set fits inside the partition, contrasting pollution.

In mode depth, \gls{i/o} activity is detrimental to \llc isolation because of the allocation patterns of \gls{dma} drivers. These drivers, indeed, reserve large amounts of contiguous memory during the system boot. Since it is impossible to predict at boot time which application (and which \llc partition) a driver will serve, \gls{dma} memory pools are not restricted to specific colors, spanning potentially all the colors. Moreover, the \gls{i/o} subsystem leverages specific interfaces for allocation, which are outside the buddy subsystem. Therefore, if repeated \gls{i/o} activities happen when the target is running, it experiences pollution because of accesses to buffers. Furthermore, when the memory reserved to the target increases due to a larger \llc partition, the probability of overlapping with buffers data in \llc and of incurring in pollution increases. This explains the increase in the miss rate with more than 4 MB of \llc. Pollution due to buffers is visible by comparing the plots in \cref{fig:app_char_nehalem} with those of the following sections, whose workloads have less intensive \gls{i/o} activity.

Concerning the slowdown curve, it seldom follows the stand-alone behavior closely: even in the case of a regular pattern like W2, the red lines are well distinct. This is due to non-partitioned resources, like the on-chip interconnection and the memory bandwidth, which are still shared among co-running applications and cause uncontrollable contention. This is especially evident for W2, where the target application, omnetpp, maintains a high miss rate (above 50\%) that causes frequent accesses to main memory and to the on-chip interconnection, experiencing contention. Instead, bzip2 has a much lower miss rate and a slowdown that is close to the optimal profile, since most of its data remain inside the \llc and the contention on the memory bandwidth is limited.

\subsection{Co-location in Sandy Bridge}
\begin{figure}
\includegraphics[width=1.1\textwidth]{figures/results/sandy_workload_w.pdf}
\caption{Profiles of the workloads in \cref{tab:spec_wl_w} on Sandy Bridge, with different cache partitions}
\label{fig:sandy_workload_w}
\end{figure}
Similarly to the previous section, \cref{fig:sandy_workload_w} shows the profiles when running on \mysandynobrand. Here we notice slightly less improvements due to \system, since the \llc has greater performance than Nehalem's, in particular higher capacity and associativity. However, isolation is still fundamental to meet a strict performance requirement.

Comparing the co-location profiles with the stand-alone references, we note similar behavior to \cref{fig:nehalem_workload_w}. Very regular applications like bzip2 and omnetpp still have comparable curves with respect to the stand-alone execution, while other applications suffer from pollution in an unpredictable way. Overall, these results on Sandy Bridge confirm our findings with Nehalem, still highlighting the role of \gls{i/o} with respect to strict \llc isolation.

\section{Isolation with more regular workloads} \label{sec:coloc_x}
After the findings of previous sections, in order to test \system's effectiveness we choose four new workloads with lower \gls{i/o} activity. These workloads derive from the those in \cref{tab:spec_wl_w} by replacing the applications identified as more polluting with others, performing less \gls{i/o}. \Cref{tab:spec_wl_x} shows the four new workloads, emphasizing the new polluters.
\input{chapters/spec_wl_x}
The choice of the applications is determined by their execution time: the new polluters have an execution time that is roughly equal to or longer than the target's. In particular, the target of W8, sphinx, has maximal duration with respect to any other application: hence, we chose another instance of sphinx as polluter as the only way to limit \gls{i/o} activity during the target's execution.

Repeating the measurements with the same methodology of the previous section, we also profiled the workloads in \cref{tab:spec_wl_x}. \Cref{fig:nehalem_workload_x} plots the workloads' profiles on Nehalem, while \cref{fig:sandy_workload_x} plots the profiles for Sandy Bridge.

\begin{figure}
\includegraphics[width=1\textwidth]{figures/results/nehalem_workload_x.pdf}
\caption{Profiles of the workloads in \cref{tab:spec_wl_x} on Nehalem}
\label{fig:nehalem_workload_x}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{figures/results/sandy_workload_x.pdf}
\caption{Profiles of the workloads in \cref{tab:spec_wl_x} on Sandy Bridge}
\label{fig:sandy_workload_x}
\end{figure}

In general, the plots show a more regular behavior of the targets, whose curves are now monotonically decreasing and closer to the stand-alone execution, with the only exception of X3 in Sandy Bridge. A posteriori, this justifies the workload choice after the findings of \cref{sec:coloc_w_nehalem}, and highlights the effect of \gls{i/o} on running applications, even if isolated within an \llc partition. In particular, X4 has steeply descending curves, which clearly indicate the presence of contention with the polluters; this contention is due to the different polluter, which is another instance of sphinx. On Sandy Bridge, such effects are even more visible, with the slowdown falling out of the plot. Looking at X3 and X4 in both \cref{fig:nehalem_workload_x} and \cref{fig:sandy_workload_x}, is evident how contention on the \llc hinders the targets' execution and, consequently, how \system can provide guarantees on the execution on a per-demand basis that are unachievable without \llc partitioning. Similar considerations apply to X1 and X2, even if the benefits are smaller, due to the better locality of their targets that the cache can leverage. Focusing on Sandy Bridge, the curves of X3 and X4 are steeper with respect to Nehalem's for the reasons explained in \cref{sec:app_char}, indicating that \system can provide even higher benefits.

\section{Overall results} \label{sec:results}
Throughout the previous sections, we measured how \system is effective in isolating applications in the \llc, allowing to control their performance on an on-demand basis. Despite the effects of \gls{i/o}, \system can positively affect running applications, in particular in the cases where the isolated application has an access pattern that cache-unfriendly. In this case, indeed, the \llc is not able, per se, to optimally manage the application's data, and contention can severely worsen the final performance. This is particularly evident with the Sandy Bridge architecture, where a controlled \llc partition brings benefits in terms of cache space and of increasing associativity (as from \cref{sec:app_char_sandy}).
