\chapter{Reconstructing Sandy Bridge hash function} \label{chap:sandy_reverse}

\epigraph{
\itshape
Tell me, o Muse, of the man of many devices, who wandered full many ways after he had sacked the sacred citadel of Troy. Many were the men whose cities he saw and whose mind he learned, aye, and many the woes he suffered in his heart upon the sea, seeking to win his own life and the return of his comrades.
}{\textit{Homer, Odyssey I}\\translated by A.T. Murray}

\Cref{sec:sandy} explains the novel architecture of Intel's Sandy Bridge family, which goes almost unchanged also through the following Ivy Bridge and Haswell families. For our purposes, the key innovation is the aforementioned introduction of a hash-based addressing scheme, that computes the \gls{llc} slice a cache line maps to from the line address. Since this hash is undocumented, the control over the \gls{llc} \system needs, as from \cref{sec:sandy_part}, is reduced to the extent that it is impossible to control the slice data are mapped to, resulting in only 4 partitions available. Therefore, in the same \cref{sec:sandy_part} we assumed the knowledge of such hash function, as it is fundamental for \system's goals.\\
This chapter, thus, presents the methodology we used to reconstruct this information, also unveiling some inner details of the \gls{cmp} model we worked with. In particular, \cref{sec:rec_method} explains the assumptions we leveraged and, consequently, the methodology we followed to reconstruct the hash function. Then, \cref{sec:rec_setup} explains how the experiments were conducted, in particular how the hardware and software were configured. \Cref{sec:rec_colliding} shows the first tests performed on our \gls{cmp}, which are interpreted in \ref{sec:rec_results} to find 6 possible hash functions that fit with the measurements. \Cref{sec:rec_myhash} shows another set of tests performed to finally find the hash of our \gls{cmp}. Based on the findings of this last section, \cref{sec:rec_topology} infers additional details about the architecture.


\section{Assumptions and methodology} \label{sec:rec_method}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/reconstructing_sandy/ralph_sandy_hash.pdf}
\caption{Reconstructed hash function of \ralphcpu}
\label{fig:ralph_hash}
\end{figure}

Reconstructing a hash function can be  very complex task, in particular with cryptographic hashes. Yet, these hashes, if implemented in hardware, have an area cost and a latency that are not compatible with the requirements of a component inside a \gls{cmp} core: indeed, Intel's hash function must work at more than 3 GHz frequency and must provide the result within less than 30 cycles, the average latency of an \gls{llc} hit in Sandy Bridge. Therefore, this hash must be simple and have small area and power overhead.\\
\textcite{hund} unveiled the hash of a \ralphcpunobrand, showing that it is basically a XOR operation on bits of the tag, as \cref{fig:ralph_hash} depicts. Since the hash function $ h $ must return a number from 0 to 3, its output uses two bits, each one computed according to a different XOR-based hash: in positional notation, $ h(b) = h_2(b) h_1(b) $. A posteriori, we could try to infer the rationale of this design choice. A first reason is that the XOR operator has evenly distributed output values, thus without biasing the hash and, thereof, the slice choice: all the 4 configurations have equal probability. Secondly, XOR ports have very small latency, area and power overhead, in particular with Intel's full custom \gls{asic} lithography.\\

Thanks to these characteristics, we can expect this design choice to hold across all the models of the Sandy Bridge family, and, with good confidence, also across other families and models. The bits used within each hash can change, but we assume the XOR operator to be the only one Intel uses. We have to stress this is a critical assumption: if it was wrong, then much more complex techniques than those we use here would be necessary. But our findings finally proved our assumption to be correct.

Once the bit-combining operator is known, the remaining task is to discover \emph{which} bits $ h_1 $ and $ h_2 $ combine. Since our \gls{cmp} model, a \mysandynobrand, is different from that in \cite{hund}, we cannot assume the hash function to be exactly the same. However, exploiting the few hints in \cite{hund}, we can learn from their reconstruction methodology to reach the same result with our \gls{cmp}.\\
Therefore, the main step consists in finding collisions among memory addresses. Using Intel's performance counters, we can detect whether an address causes an \gls{llc} miss \cite[chapter~18]{intel_man3}.
By reading proper sequences of addresses (as detailed in the following), we can find whether an address has evicted another one: in such case, the two addresses collide, hence are mapped to the same \gls{llc} set and must have equal hash. Following the procedure in \cite{hund}, we will collect groups of memory addresses that collide in the \gls{llc}. In particular, given a fixed memory address called \emph{prober}, we find several other addresses, called \emph{colliders}, that collide with the prober. Then the comparison of these addresses let us infer the influence of each bit over $ h_1 $ and $ h_2 $.


\section{Environment setup and preliminary considerations} \label{sec:rec_setup}
The machine \textcite{hund} use ships 4 GB of \gls{ram}: this limits the physical memory bits to 32, so that the hash function works with bits from 0 to 31. Higher bits are 0 and, even if they are used in the hash, they do not affect the XOR operator. Instead, the machine we use for the measurements has 6 GB of \gls{ram} and uses thus also bit 32: to learn the influence of this bit too, we have to extend the collection of colliding addresses also to the upper 2 GB of memory, where this bit is set.

The necessity of knowing physical memory addresses forced us to make measurements with ring-0 privilege level, thus inside an \gls{os}. We chose Linux, which allows users to insert executable \emph{modules} at runtime and provides good documentation and interfaces for a deep control over the \gls{os} and the hardware, even from external modules\cite[chapter~7]{Mauerer:2008:PLK:1502342}. Another key functionality of Linux was the possibility of accessing the whole memory space with ease, since the x86\_64 version of Linux maps the entire physical memory of the machine to the kernel address space, thanks to the huge availability of virtual memory with 64 bit machines \cite{linux:64bit:vmm}.

Since our aim is to collect the \emph{exact} memory addresses that collide with each other, it is fundamental to have very precise measures. Yet, in today's architectures, many features can potentially disturb the measurements, leading to noisy results that are useless for our goals.
Moreover, Intel's performance counters are not conceived for measurements of high precision, but to characterize long execution phases of applications (in the order of milliseconds or more), when the hardware monitors typically count millions of events: for this purpose, differences between the counters and the ``true'' values of few hundreds of events are negligible, while in our testbed they would totally invalidate our results. Therefore, it is absolutely necessary to disable all sources of noise in the testbed.

A first example of a disturbing feature is speculative prefetching, which loads data according to forecasts based on the memory access pattern. In particular, modern architectures have prefetching units in the pipeline front-end, to prefetch instructions from the \gls{l1}, and inside each layer of cache, to prefetch lines from the upper memory layer. Prefetchers can disturb measurements by pre-fetching unneeded lines that evict other lines, thus increasing the miss counter. Since it is not possible to know which address caused the miss, misses due to prefetchers could be confused with those artificially caused by out tests, thus invalidating the measurements. In our machines, it is possible to disable cache prefetchers from the configuration panel of the \gls{bios}, while instruction prefetchers cannot be disabled. However, since they prefetch from the \gls{l1}, we expect them not to affect the measures.

Sandy Bridge \glspl{cmp} aggressively adjust the frequency based on the cores utilization through \gls{dvfs}, with a hardware/software control. To stabilize the measurement environment and limit unpredictable behavior like event loss or delays, we set in software the minimum frequency and disable any power-saving mechanism. Although the hardware may still decide to make small \gls{dvfs} regulations independently, we assume these actions to be limited and not to pollute measurements sensibly.\\
Since our test kernel module runs on a single core, other cores are a major source of noise: in fact, they simultaneously run other applications, system services and interrupt handlers (like the clock interrupt handler, that drives preemptive kernels). According to our experience, it is fundamental to disable the other cores to achieve much higher predictability and precision; for the same reason, all interrupts on the working core are disabled when the module is loaded.

Modern \glspl{cmp} are designed to aggressively execute instructions out of order, leveraging speculative mechanisms that proved to be very effective. These features often change the order of instruction execution in order to hide pipeline and memory latencies. Since it is impossible to know which memory address caused the miss event we eventually measure, we can rely only on the memory accesses we trigger by software to understand which memory address caused the miss event. Hence, we have to enforce the execution of memory accesses in exactly the same order they appear in software. Since it is impossible to disable speculative, out-of-order execution, we can only attempt to enforce an in-order execution. Therefore, we disable all the compiler optimizations, which reschedule instructions, we force each memory access by the C \code{volatile} qualifier and we explicitly protect accesses with a hard memory barrier (the x86 \code{mfence} instruction\cite[section~3.2]{intel_man2}), which forces both the compiler and the pipeline to maintain the order of memory accesses.\\
Finally, we learned from our experience that Intel's performance counters might have delays in the updating the register values, in particular with consecutive accesses. Still from our experience we learned that, most of the times, the register is updated as expected after multiple reads. Despite all these precautions, we experienced that some noise is inevitable.

We made the tests taking into account all these details, writing the modules' code with care, barriers and pointer access modifiers, and also checking the output assembly code. However, in describing the tests we made we will omit to show all these precautions, not to overwhelm the reader with an amount of details that can hide the basic concepts.

\section{Collecting colliding addresses} \label{sec:rec_colliding}
Finding two colliding addresses in the \gls{llc} requires considering all its parameters. Looking at \cref{fig:ralph_hash}, we notice that bits 6 to 16 are used for the set index and bits 0 to 5 are used for the line. Therefore, two addresses that have bits from 0 to 16 in common map to the same set number and line offset. To compute these addresses, we use a parameter called \emph{stride}, which is equal to $ 2^{17} $: two addresses are mapped to the same set number if their distance is a multiple of the stride. Given a probe address, we can find multiple colliders by repeatedly summing the stride to the probe and checking whether the probe is evicted after every access. Yet, adding the stride increments the tag, which is the input of the hash function: therefore, two addresses differing of a stride are mapped to the same set number, but might unpredictably be mapped to different slices, without colliding. Hence, the number of memory locations we have to traverse to find a collision is not a priori known. Since the associativity of the \mysandynobrand is 20, in the best case 20 memory locations with offset equal to the stride can map to the same set inside the same slice, filling the set; in such case, if also the 21st memory location maps to the same slice, it evicts the probe address, and we can detect this event by reading the probe and checking whether a miss happens. However, this scenario is impossible in practice, because a hash function like that of \cref{fig:ralph_hash} changes frequently with the lower bits. Thereof, we expect the data we access to be evenly distributed among the 4 slices: in this more realistic case, 78 accesses are needed on average to fill a set, and about 80 to evict the probe and trigger a miss when the probe is subsequently read. And, in our measurements, we found very close numbers.

We can summarize this test with the following code snippet, which looks for a collider by checking whether the global probe has been evicted.

\begin{lstlisting}[style=pseudo]

globaldata:	integer stride
			address probe
			integer collider_offset

procedure FIND_COLLIDER
input:	none
output: address
behavior:
	integer i, miss_count, jumps
	address first_collider
	
	// compute address of first collider
	first_collider = probe + collider_offset * stride

	for jumps from 20 on
		// bring probe into LLC
		READ_ADDRESS(probe)
		
		// bring colliders into LLC
		for i in [0,jumps)
			READ_ADDRESS(first_collider + i * stride)
		
		miss_count = READM_MISS_COUNT()
		READ_ADDRESS(probe)

		if (READM_MISS_COUNT() > miss_count)
			// last collider hash evicted the probe
			return first_collider + i * stride
end procedure
\end{lstlisting}

For the sake of simplicity, we wrapped the low-level details with the functions \code{READ_ADDRESS}, which reads the memory address with the hard barries, and \code{READM_MISS_COUNT}, which reads the miss counter register using Intel's dedicated performance counter \cite[chapter~19.5]{intel_man3}.

By changing the variables \code{probe} and \code{collider_offset}, we can find multiple couples probe \hyphen collider. In particular, by varying only \code{collider_offset} we can find multiple colliders associated to the same probe, which are useful for the following steps. Furthermore, by adding to each probe and each collider an additional, constant offset lower than the stride, we can map addresses to a given set number, not necessarily 0, collecting conflicting addresses with the lower 17 bits being not all 0. This will let us verify that bits lower than 17 are not used in the hash, as \cref{fig:ralph_hash} found. Overall, with these experiments we collected several probes $ p_{a} $, $ p_{b} $, etc. and multiple colliders for each probe, represented as sets $ \{ c_{a,1} c_{a,2}, ... \} $, $ \{ c_{b,1}, c_{b,2}, ... \} $, etc., accounting for more than 2'7000'000 millions addresses.

\section{Results interpretation} \label{sec:rec_results}
With these data, we can have a first insight on the hash bits and on the measurement noise. To evaluate how noisy the measures are, we perform a first test on our data. Considering a single probe $ p $ and its colliders $ \{ c_{1} $, $ c_{2}, ... \} $, we know that the hash of all these addresses are equal as they conflict in the \gls{llc}. If we consider any two colliders $ c_i $ and $ c_j $ of the same probe, their Hamming distance $ H(c_i,c_j) $ cannot be 1, that is to say they cannot differ for a single bit: if this happened, the hash would be different, because $ h_1 $, $ h_2 $ or both would change because of this only bit. Thus, finding couples of colliders of the same probe that have Hamming distance of 1 gives an idea of the noise affecting the measurements. In our experiments, less than 0.03\% of the measures fulfilled this condition; once two such colliders are found, they are discarded.

Assuming that all the bits 17 to 32 are used within at least one of the two hash functions, as in \cite{hund}, the problem of reconstructing the hash $ h = h_2 h_1 $ may be reduced to a clustering problem. Specifically, we expect only three clusters of bits:
\begin{enumerate}
\item the cluster of bits only in $ h_1 $
\item the cluster of bits only in $ h_2 $
\item the cluster of bits both in $ h_1 $ and in $ h_2$
\end{enumerate}
To find these clusters, we look for colliders of the same probe having Hamming distance of 2. Since these colliders have the same hash, we infer that the two changing bits do not change the overall hash. This is due to the property of XOR: if two bits change at the same time, their XOR does not change. This, in turn, implies that the two bits are in exactly one of the following \emph{configurations}:
\begin{enumerate}
\item both bits are used in $ h_1 $
\item both bits are used in $ h_2 $
\item both bits are used both in $ h_1 $ and in $ h_2 $
\end{enumerate}
For each couple of colliders at Hamming distance 2 we found the two different bits $ i $ and $ j $, and counted how many times $ i $ and $ j $ appeared across the whole dataset. This count represents the \emph{likelihood} $ l_{i,j} $ of bits $ i $ and $ j $ to be in the same configuration.

In a similar way, we need an estimation of the \emph{unlikelihood} $ u_{i,j} $ of bits $ i $ and $ j $ to be in the same configuration. The higher the unlikelihood, the higher is the probability that $ i $ and $ j $ are not in the same configuration. To find such couples, we can consider two probes $ p_a $ and $ p_b $ with Hamming distance 1, thus guaranteed to have different hashes, and two colliders $ c_{a,i} $ of $ p_a $ and $ c_{b,j} $ of $ p_b $ with Hamming distance 2. Since the hashes of $ c_{a,i} $ and $ c_{b,j} $ must be different, the two different bits must have a different configuration, otherwise they would ``flip'' the XOR twice and cause the hash values to be equal. Counting the occurrences of such couples, we devised an estimation of the unlikelihood $ u_{i,j} $ for each couple of bits.

Finally, likelihood and unlikelihood allow us to cluster bits based on their configuration. To this aim, we developed a simple clustering based on an \gls{ilp} model. Introducing the binary variable $ s_{i,j} $ that represents whether bits $ i $ and $ j $ are in the same cluster, we can write the objective function as
\[
\mbox{minimize} \sum_{i=17}^{32} \sum_{j=i}^{32} [ ( 1 - s_{i,j} ) + l_{i,j} - s_{i,j} \times u_{i,j} ]
\]

To indicate whether a bit $ i $ is inside cluster $ c $, we also introduce the binary variables $ t_{i,c} $, and add the constraint
\[
s_{i,j} \geq t_{i,c} + t_{j,c} -1 \qquad \forall c \in [1,n]
\]
where $ n $ is the number of clusters.
Finally, since a bit must stay in a cluster only, we also add the constraint
\[
s_{i,j} \leq 1 - t_{i,c} + 1 - \sum_{ k in [0,n] \setminus \{c\} } t_{j,k} \qquad \forall c \in [1,n]
\]
Trying with different values of $ n $, three clusters of bits emerged:
\begin{gather*}
f_1 = {18, 25, 27, 30, 32} \\
f_2 = {17, 20, 22, 24, 26, 28} \\
f_3 = {19, 21, 23, 29, 31}
\end{gather*}
Bits 6 to 16 have very similar likelihood and unlikelihood in every couple they appear, but the values are very low compared to those of other couples, as representing occurrences of noise. This confirmed the independence of the hash from bits lower than 17.

\section{Finding the correct hash function} \label{sec:rec_myhash}
The three clusters found are in accordance with the configurations in \cref{fig:ralph_hash}; however, we still do not know which configuration each cluster corresponds to. Indeed, any cluster may correspond to any configuration, and the two hash functions $ h_1 $ and $ h_2 $ are the XOR-combination of a common and a specific configuration. Therefore, there are 6 possibilities, corresponding to 6 different hashes $ h $. If we denote with $ x_i $ the XOR of all bits in $ f_i $ and separate with the comma the two functions $ h_2 , h_1 $ (to keep the usual positional notation), the 6 hashes are:
\begin{align*}
h_a = (x_1 ~ XOR ~ x_2) , (x_3 ~ XOR ~ x_2) \\
h_b = (x_1 ~ XOR ~ x_3) , (x_2 ~ XOR ~ x_3) \\
h_c = (x_2 ~ XOR ~ x_1) , (x_3 ~ XOR ~ x_1) \\ \\
h_d = (x_3 ~ XOR ~ x_2) , (x_1 ~ XOR ~ x_2) \\
h_e = (x_2 ~ XOR ~ x_3) , (x_1 ~ XOR ~ x_3) \\
h_f = (x_3 ~ XOR ~ x_1) , (x_2 ~ XOR ~ x_1) 
\end{align*}
where the last three hashes are obtained by ``swapping'' the first three ones. If we closely look at these functions, we notice that it is possible to map each function to another one in a bijective way. This because, once we know the result of a hash, we can uniquely derive the values of $ x_1 $, $ x_2 $ and $ x_3 $, and from these values compute any other hash. This implies that a bijection between any two hashes exists, further implying that these functions have the same output distribution, thus being equal in terms of ``spreading'' capability. Hence, Intel could use any of these functions in its \glspl{cmp}: for example, \textcite{hund} showed that \ralphcpunobrand uses $ h_e $.

To find which hash function our \gls{cmp} uses, we decided to exploit the variable latency of the ring interconnection: trying a specific hash function, we can compute in which slice an address is supposed to be mapped. Then, we verify if this hypothesis is correct: for example, if the test is done on core 0 \footnote{Linux assigns several identifying numbers to cores, some directly coming from the hardware and others based on \gls{smt} capabilities and boot order, in turn depending on the \gls{bios}. Here we use the hardware ID of a core, which follows the same numbering of the slices and is called \emph{ApicID} in Linux.}, accessing slice 0 must have minimum latency with respect to the other slices. In this way, we can check whether a specific hash is correct.

Therefore, we wrote another Linux module to measure the latency when accessing a given memory address. This module has to access the \gls{llc} and measure the access latency, without accessing the \gls{l2}. To do this, we have first to fill the \gls{llc} with the data we want to read and then fill the \gls{l2} with other data from the \gls{llc}, so that the measured access will hit in \gls{l3} and not in \gls{l2}. The following code snippet summarizes this procedure.
\begin{lstlisting}

globaldata: address buffer
			integer llc_size
			integer l_size
			integer offset

procedure MEASURE_LATENCY
inputs: none
outputs: integer
behavior:
		integer i
		
		for i in [0, llc_size)
			READ_ADDRESS(buffer + i)
		
		for i in [llc_size - l2_size,  llc_size)
			READ_ADDRESS(buffer + i)
		
		READ_ADDRESS(buffer + offset)
		return READ_LAST_ACCESS_LATENCY()
end procedure

\end{lstlisting}

Here, \code{buffer} is a generic memory area used to read data, and \code{offset} controls which data are read. By varying this parameter of the stride value, it is possible to read data from a desired slice.

In our experience this measure was affected by a lot of noise: even reading the same address multiple times, the measured latency varies in a certain range that depends on the slice. For example, if accessing slice 0 from core 0, the latency typically varies from 18 to 23 cycles, while the farthest slice from core 0 (slice 3) has a latency in the interval from 27 to 31 cycles. These variations are likely due to the complexity of the architecture, in particular to the conflicts the core may experience on the ring bus and when accessing a slice, which could already be busy to service a prior request and enqueue the core's access. Therefore, we concentrate on the minimum values, which describe the access latency in ideal conditions. Looking at these values, we found that couples of cores have symmetrical latencies when accessing slices, and also have the same minimum latency, 18, that we assume to be the latency towards their local slice. However, the only hash that maps that maps each core's numbers to the same slice number is hash $ h_d $, shown in \cref{fig:my_hash}. This hash is different from that of \cite{hund}, probably because of the different model.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/reconstructing_sandy/my_sandy_hash.pdf}
\caption{Reconstructed hash function of \mysandynobrand}
\label{fig:my_hash}
\end{figure}

\Cref{tab:sandy_latencies} summarizes the minimum latencies found when accessing the slices from each core. We note, again, symmetry between couples of cores, guessing that the routing protocol of Sandy Bridge routes cache lines to the best direction, that is to say the one with the least hops.

\begin{table}
\centering
\caption{Minimum LLC latencies measured in \mysandynobrand when accessing a slice from a core}
\begin{tabular}{c|c|c|c|c|}
\cline{2-5}
                             & slice 0 & slice 1 & slice 2 & slice 3 \\ \hline
\multicolumn{1}{|c|}{core 0} & 18      & 20      & 26      & 27      \\ \hline
\multicolumn{1}{|c|}{core 1} & 20      & 18      & 31      & 26      \\ \hline
\multicolumn{1}{|c|}{core 2} & 26      & 27      & 18      & 20      \\ \hline
\multicolumn{1}{|c|}{core 3} & 31      & 26      & 20      & 18      \\ \hline
\end{tabular}
\label{tab:sandy_latencies}
\end{table}

\section{An educated guess: reconstructing the topology} \label{sec:rec_topology}
Analyzing the latencies, we can try to partially reconstruct the topology of the \mysandynobrand. In particular, \cref{tab:sandy_latencies} shows which are the closest slices to each core. For example, core 0 is close to slice 1, while slices 2 and 3 are more distant. Similarly, core 3 is close to slice 20 and distant from slices 1 and 0. Still looking at core 0, the difference between the latency of slice 0 and slice 1 is 2 cycles. This is likely due to the 1-hop distance the request and the response have to travel. Therefore, we can infer that there is a single hop distance between cores 0 and 1, and similarly between cores 2 and 3. Instead, the latency between, for example, core 0 and slice 2 is higher, suggesting that there are intermediate steps like memory ports, \gls{i/o} interfaces, PCIExpress interfaces, \gls{qpi} bridges for inter-processor coherency, etc. This also appplies to the distance between core 1 and core 2, where the number of hops is, probably, even higher. Moreover, we can note that, when two cores differ for only the least significant bit, they are close, otherwise they are more distant. This is a typical characteristic of routing protocols for high-bandwidth, hypercube interconnections, whose key features probably inspired Intel's engineers.

Looking at the maximum values in \cref{tab:sandy_latencies} (31), we can note that it appears only in two cases, when the communication occurs from core 1 to core 2 and from core 3 to core 0, while in the opposite cases the latency is 27. This is likely due to an asymmetry in the routing protocol, for example because it sends the response along a path different from the path of the request. This, in turn, might serve to balance the load of each ring segment, to avoid bandwidth saturation and, consequently, long latencies to access shared resources such as memory or \gls{i/o} interfaces.\\
Considering all these insights, we attempt to reconstruct the topology of \mysandynobrand in \cref{fig:my_topo}, were the intermediate hops (with dashed lines) are supposed to be between cores 1 and 3 and between cores 0 and 2. The red spots depict the cache boxes and two buses with opposite directions are assumed, as in \cref{fig:sandy_ring}.


\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/reconstructing_sandy/my_sandy_topo.pdf}
\caption{Reconstructed topology of \mysandynobrand}
The intermediate hops are dashed as their function is not known
\label{fig:my_topo}
\end{figure}
