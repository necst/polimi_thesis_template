\chapter{Design} \label{chap:design}
This chapter shows the design of our proposal, which is a modification of the Linux kernel to implement page coloring. Our solution aims to guarantee to applications  isolation in the \gls{llc} and is designed considering the architecture of modern \glspl{cmp}. In this work we propose to study the constraints these architectures pose to our page coloring and to explore the possibilities cache isolation opens, along with the necessary trade-offs.
Throughout this chapter, \cref{sec:vision} explain the overalls vision behind our work, while \cref{sec:approach} explains how the proposed solution integrates with the Linux kernel. \Cref{sec:cache_model} discusses the assumptions on the \gls{llc} at the base of our proposal and the limitations they impose on our work, while \cref{sec:consequences} shows how page coloring impacts on applications. Finally, \cref{sec:buddy_mod} explains the modifications to the buddy data structure and to the allocation algorithm.

\section{Vision} \label{sec:vision}
The final objective of this work is to allow distributed computing platforms to provide better \gls{qos} via isolation of running applications.
The principal way to ensure \gls{qos} is partitioning the hardware resources shared by co-running applications. Some techniques like time sharing mechanisms already exist from a long time, but cannot avoid the detrimental effects of contention, in particular those exposed in \cref{sec:contention}. Modern \gls{cmp} architectures, as from \cref{chap:soa}, exacerbate contention, but are ubiquitous in today's computing environments. On these architectures resource partitioning, if available, is actually limited to a coarse granularity; instead, unpartitioned resources degrade performance and cause unpredictability.
Focusing on the \gls{llc} to mitigate contention on this resource is the key point to enhance isolation, and hence performance predictability and \gls{qos} guarantees. Since we also want the proposed solution to be feasible - and possibly beneficial - in real systems, we cannot propose hardware changes, but we concentrate instead on the possible ways by which software can enforce performance isolation through \gls{llc}. Looking at the actual state of the art in \cref{chap:soa}, several techniques face \gls{llc} contention phenomena proposing different solutions, but only \emph{page coloring} provides strong guarantees of isolation even in co-location on a per-application basis. This aspect is fundamental in our choice: fulfilling \gls{qos} requisites according to \glspl{sla} requires a precise, tunable per-application control that is effective in ``crowded'' and ``noisy'' environments. Therefore, to bring page coloring to a realistic distributed computing environment, we propose a re-implementation of page coloring that is tailored to modern architectures, in particular to those shown in \cref{sec:intel_archs}.

Considering the high variability of distributed computing environment, the broad customers audience and the legacy, we assume to have no control over the single application, nor we can enforce specific programming practices or frameworks.
Ideally, we would achieve predictable performance for mixes of applications sharing a single commodity \gls{cmp}, like those powering data centers, without any modification to either runtimes (e.g., the Java virtual machine) or applications. Given these assumptions, the implementation of page coloring within an \gls{os} makes a good fit, since it requires updating only the \gls{os} and understand applications' resource requirements.

Another key decision of our solution lies in how the \gls{llc} partition size is chosen. Ideally, a production-ready mechanism should be capable of deciding the size of the \gls{llc} partitions automatically, taking into account hardware constraints and \gls{qos} requirements, in an adaptive way. However, in the context of this work we focus on providing recent commodity \gls{cmp} architectures with an \gls{llc} partitioning mechanism, implemented through page coloring. 
To demonstrate the importance of this technique even in newer architectures, we implemented a solution relying on \emph{static} partition sizes provided by the user. Thus, the solution we develop in this thesis may serve as a starting point for experimenting dynamic policies (as those in \cref{chap:soa}).
 
\section{Approach} \label{sec:approach}
Implementing page coloring requires deep modifications to the physical memory allocator, a core part of any operating system that is not configurable nor ``pluggable'' at runtime. Our focus on cloud infrastructures leads us to consider an \gls{os} that supports the diverse application and hardware scenarios these environments offer. Among others, we wish an \gls{os} that supports virtualization natively, as many cloud environments are based on this capability. Linux, for example, is released under \gls{gplv2} license and supports virtualization natively though \cite{kvm} and hardware emulation through the widespread QEMU \cite{qemu}. Other open source \glspl{os}, such as FreeBSD, do not have this native support, nor are as widespread as Linux is. Concerning Xen, the open source hypervisor, we have to note that it is capable of managing \glspl{vm} only, and not normal applications; while Linux manages \glspl{vm} as normal processes. Since we desire to provide isolation capabilities to any kind of process, either a native application or a \gls{vm}, the most natural choice is Linux.

The choice of the Linux kernel, thus, drives our design. The solution we designed, called \system, introduces page coloring into Linux' buddy allocator, accounting for the aforementioned objectives. As from \cref{sec:buddy}, the physical allocator is a critical component: it is accessed frequently and is involved in the page fault mechanism that allows Linux to grow and shrink the physical memory of a task (i.e., process, thread or \gls{vm}). Therefore, any modification must be carefully designed, in order to continue meeting the allocator's goals exposed in \cref{sec:buddy} while affecting only the minimum possible amount of kernel code.

Although the core of \system affects the internal memory allocator, we need to provide an easy-to-use and flexible interface to applications. Considering the possibilities Linux offers, we chose to develop a \emph{cgroup} \cite{cg}, providing userspace applications with a filesystem-like interface to request an \gls{llc} partition.

In the following we explore the main design decisions behind \system, starting from how modern \gls{cmp} architectures of \cref{sec:intel_archs} affect page coloring.


\section{Cache hierarchy model and page coloring} \label{sec:cache_model}
This section explains the major assumptions about the cache hierarchy at the base of \system. In particular, \cref{sec:assumptions} goes through these assumptions in the context of a generic \gls{cmp} architecture with multiple layers of cache. Given these assumptions, \cref{sec:nehal_part} and \cref{sec:sandy_part} explain the consequences of page coloring adoption on the caches hierarchies of Nehalem and Sandy Bridge architectures.


\subsection{Assumptions on the cache hierarchy} \label{sec:assumptions}
Devising a model of our target cache hierarchy is an essential task to go through the design of \system. On one side, this model reflects the cache hierarchies available in commodity \glspl{cpu}, such as the layering of caches and their interconnection with the cores. On the other side, it should be general enough to be applied to a wide class of commodity \gls{cmp} architectures.

The first assumption is that the architecture features a unique \gls{llc} shared among all the cores, with a set-associative indexing scheme. This assumption is verified in most of today's commodity \glspl{cmp}, with the exception of few models that are however rare. Moreover, the most recent architectures, like Sandy Bridge, are designed according to this model. Instead, lower levels can also have a different architecture, for example a Harvard architecture with separate caches for code and data, without impacting our design. Instead, the line size is assumed to be constant across the layers of the hierarchy, as verified in all \gls{cpu} architectures.\\
Another fundamental assumption is that the \gls{llc} be a \gls{pipt}-addressed cache. This allows the control of the data placement in the \gls{llc} on behalf of the physical allocator, a key requirement in our vision.

Together, these assumptions allow the physical memory allocator to control \emph{all} the applications co-running on the \gls{cmp} cores in a way that is totally transparent to the user.

These assumptions fit a wide variety of commodity architectures, by Intel, ARM and IBM, in addition to those we target in this work. Some architectures like IBM Power6 and Intel Crystalwell (a variant of Haswell) also have an higher layer of cache used as a victim cache, which is outside our model. Because of the different nature of these memories, which act solely as a victim buffer for the lower levels of the hierarchy, modeling them is not needed for the design we propose.

In addition to these assumed common features, modern \glspl{cmp} use \gls{pipt}-indexed caches also in lower levels, thanks to the small latency of their \glspl{tlb}. These caches are typically per-core \gls{l2} caches, while \gls{l1} caches are often \gls{vipt}-indexed, having strict latency requirements to feed the core pipeline.\\
Overall, these features, being commonly met in recent commodity \glspl{cmp}, are taken as a reference model throughout our design, and possible variations do not have a strong impact, as discussed in the following sections.

\subsection{Partitioning Nehalem's hierarchy} \label{sec:nehal_part}
Recalling \cref{sec:coloring}, page coloring is the only software technique able to partition the \gls{llc}; it exploits the address bits in common between the \gls{llc} set and the page address for partitioning, as \cref{fig:coloring} depicts.
Nehalem \glspl{cmp}, having a classical \gls{llc} addressing scheme, follow this model and can be partitioned in the same way as previous \glspl{cpu}. Therefore, the page coloring implementation used throughout the state-of-the-art work of \cref{sec:coloring} could theoretically be employed.\\
Yet, cache allocation is subject to another constraint. Assuming three layers of caches, partitioning the \gls{llc} could impact also the use of the \gls{l2} cache. \cref{fig:addr_cachel2} shows the use of the physical address to access the \gls{l2} cache of our target platform, which differs from \cref{fig:addr_cache} as the set number consists of 9 bits instead of 13.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/design/addr_cachel2.pdf}
\caption{Physical address bit fields for \gls{l2} cache access}
\label{fig:addr_cachel2}
\end{figure}

Among these bits, some overlap with the \gls{llc} color bits, as in \cref{fig:color_overlap}, where the 3 most significant bits of the \gls{l2} set number are in common with the \gls{llc} color bits.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/design/color_overlap.pdf}
\caption{Overlap of color bits and L2 set bits in a \mycpu}
\label{fig:color_overlap}
\end{figure}

This overlap causes also the \gls{l2} cache to be partitionable through the subset of common bits we identified. Partitioning this level of cache can penalize the running thread because it would restrict the the data it can keep inside this layer; this, in turn, increases the accesses to the \gls{llc} and hence the overall memory access latency. Even with \gls{smt}-enabled cores, partitioning the \gls{l2} cache can be a detrimental operation, since it is very hard to balance the partition sizes, and an unbalanced partitioning may result in dangerous bottleneck effects. Therefore, in this work we choose not to partition this layer, thus preventing the use of the common bits. This decision is another key characteristic of \system, that ensures the full exploitation of the lower cache levels capacity in \gls{cmp} architectures. Thus, the remaining, highest color bits are those effectively usable for \gls{llc} partitioning, and are 4 in the example of \cref{fig:color_overlap}. This limits the number of partitions to 16, a granularity that, though not fine, is enough for the purpose of this work.

Unlike the \gls{l2} cache, the lowest \gls{l1} cache (assumed to be doubled for data and instructions) is usually virtually indexed, so that partitioning the upper layers does not affect the data placement inside this layer. However, even if this layer was physically indexed, its size is very limited, so that the set bits of both \gls{l1} caches are fewer than those of the \gls{llc}, with no overlap.

Exploiting mathematical formalism, we can finally define the notion of page color for the \mycpu of \cref{fig:color_overlap} in order to identify the single \gls{llc} partitions of minimal size. If we represent the $i$-th bit of the address $ b $ in \cref{fig:color_overlap} with the notation $ b_i $, thus $ b = b_{47}b_{46} ... b_{0}$ with positional notation \footnote{physical addresses are composed of at most 48 bits in today's machines}, for the Nehalem architecture we can define the color as a function $ c : \{ 0 ~...~ 2^{48}-1 \} \rightarrow \{ 0 ~...~ 15 \} $ of the address in the following way:
\[
color_N(b) = b_{18} b_{17} b_{16} b_{15}
\]

\subsection{Partitioning Sandy Bridge's hierarchy} \label{sec:sandy_part}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/design/ralph_memory_address.pdf}
\caption{Utilization of address bits in \ralphcpu}
\label{fig:sandy_address}
\end{figure}

With Sandy Bridge, Intel adopted a hash-based \gls{llc} addressing scheme, as discussed in \cref{sec:sandy}. Based on the physical address, the hash function computes the \gls{llc} slice the line resides in. The hash function is undocumented, but past work has unveiled interesting details. In particular, \textcite{hund} reconstruct the hash function of a specific Sandy Bridge model for security purposes, showing that the hash employs all of the higher bits as input. In \cref{fig:sandy_address}, representing the \ralphcpu \gls{cmp} used in \cite{hund}, bits 17 to 31 are used for the hash computation, while bits 6 to 16 address the index inside a specific slice. Wishing to use the same coloring mechanism devised for Nehalem, we must take in account the parameters of the \gls{l2} cache, which is identical to that of Nehalem \glspl{cmp}. Hence, \cref{fig:sandy_colors} shows that bits 12 to 14 still overlap with the \gls{l2} set index, so that only bits 15 and 16 are available for partitioning, with 4 possible partitions inside each slice. Since the hash function maps an address to a slice in an unpredictable but statistically fair way, even controlling bits 15 and 16 any process requiring more than $ 2^{16}=64 KB $ of memory can receive any slice, because the bits higher than 16 vary. Thus, any process with a realistic amount of memory, if colored by means of bits 15 and 16, would receive the same partition on all the slices (4 in the case of the \ralphcpunobrand). Therefore, the final number of controllable partitions would be 4. This is, yet, a too coarse granularity for an environment with multiple, different processes such as the one we target.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/design/ralph_coloring.pdf}
\caption{Color bits in \ralphcpu}
\label{fig:sandy_colors}
\end{figure}

To have a better control over the partitioning, we must assume the knowledge of the hash function, which can be found in a similar way to \cite{hund}. With this information, we can control both the cache sets inside the slice and the slice itself, for a total of 16 partitions, an acceptable granularity. To devise a more formal model, we can define the hash function $ h $ as a function from the address space to the set of slices: $ h : \{ 0 ~...~ 2^{48}-1 \} \rightarrow \{ 0 ~...~ 3 \} $. Using a positional notation, which is more useful in our context, the hash function can also be represented as $ h(b) = h_2(b) h_1(b) $, where $ h_1 $ and $ h_2 $ are the specific hash functions that compute each bit, as in \cite[page~198]{hund}. Thus, we can define the color also for a Sandy Bridge platform by concatenating the hash function and the per-slice color bits, in the following way:
\[
color_{SB}(b) = h_2(b) h_2(b) ~ b_{16} b_{15}
\]
In this way, with this notion of color in a Sandy Bridge \gls{cmp} we uniquely identify each partition inside a slice by means of bits 15 and 16 and each slice by means of bits $ h_1 $ and $ h_2 $.

For the sake of generality, this notion could be extended in a similar way to different \glspl{cmp} than \ralphcpunobrand; for example, for an eight-core \gls{cmp} the hash function is defined as $ h(b) = h_3(b) h_2(b) h_1(b) $, and the color definition immediately follows. Studying the characteristics of recent Intel's \glspl{cmp}, we find that many features such as the number of per-slice sets (2048 in \ralphcpunobrand) and the number of \gls{l2} sets do not vary, even if the number of cores does; thereof, bits 15 and 16 always identify the per-slice partition. This makes the definitions in this section general enough to model many \glspl{cmp} without further effort.

Finally, the assumption of knowing the hash function is hardly satisfied in practice. Nonetheless, it is possible to reconstruct this information as \textcite{hund} did and \cref{chap:sandy_reverse} explains a repeatable methodology to find this function. However, generally, it is not possible to rely on single attempts of reconstruction in order to build a \gls{cmp}-abstraction framework embedded within the \gls{os}, as a production-ready implementation would require. To this final aim, the cooperation of \gls{cmp} manufacturers is fundamental.


\section{Consequences of page coloring on applications} \label{sec:consequences}
The strict dependence of the \gls{llc} mapping on the physical page address due to the \gls{pipt} addressing scheme has several consequences. If a task receives an \gls{llc} partition, becoming a \emph{colored} task, it undergoes some limitations, which this section discusses. In particular, \cref{sec:consequences_mem} shows how page coloring limits the physical memory available to the colored task, while \cref{sec:consequences_hugep} shows why hugepages become unavailable with page coloring.

\subsection{Cache-Memory constraint} \label{sec:consequences_mem}
Page coloring essentially consists in choosing certain memory pages to control data mapping to the \gls{llc}. Therefore, if a task receives a set of colors the physical allocator can choose only the pages of those colors to fulfill the task's memory requests. Thus, the task is inherently associated those pages, which are a subset of the entire memory of the machine. This means that, if a task has already consumed all these pages and requires further memory, its requests cannot be satisfied, unless using pages from other colors. But this last possibility causes the tasks to use \gls{llc} areas that are used by other tasks, which gives lieu to contention over the shared \gls{llc} sets.\\
As \system aims to enforce strict isolation within the \gls{llc}, a key design decision we made is to use only the user-reserved colors. Thus, since \system assumes the number of colors (and, implicitly, the corresponding memory pages) as a user's input, it is fundamental that this input be properly chosen with respect to the memory footprint of the task, in addition to the desired final performance.

If the user underestimates the number of colors, the allocator cannot satisfy the requests and upper kernel layers may decide to swap memory pages to a disk in order to perform the allocation. But this operation has an overhead that is typically intolerable. Furthermore, if the pressure on the kernel memory subsystem increases over a certain limit, the allocation might fail and the process terminate erroneously.

Quantifying the memory percentage an \gls{llc} partition reserves to a process is simple in the case of the Nehalem platform.
If $ N $ is the number of memory pages and $ C $ is the number of colors (always a power of 2 for both Nehalem and Sandy Bridge, as from \cref{sec:nehal_part} and \cref{sec:sandy_part}) and $ N $ is a multiple of $ C $, then each color is associated to exactly $ n = \frac{N}{C} $ pages. Hence, a process that receives $ c $ colors can allocate over $ \frac{N}{C} \times c $ pages. Thereof, the percentage of memory a process can use is equal to the percentage of \gls{llc} it receives from the user.\\
The assumption that $ N $ is a multiple of $ C $  is a good approximation of reality: indeed, the \gls{ram} memory in modern machines is provided in sticks of relatively big capacity (around 1, 2, 4 GB of more), this amount being very close to a power of 2. Therefore, the single stick holds a number of pages that is, approximately, a power-of-2 multiple of $ C $, and more sticks, even if of different sizes, sum up to a page amount that is a multiple of $ C $. A more precise quantification would require considering the exact size of each memory stick, which depends on the vendor and the model. However, since colors repeat over the memory pages due to the color bits being a subset of the page address, the differences among colors are negligible in today's machines with millions of pages.

In the case of a Sandy Bridge platform, it is not possible to compute the number of pages per color in advance, because the hash function controls the bits $ h_1 $ and $ h_2 $ in an unpredictable way. Nonetheless, we can assume the output of the hash function to be evenly distributed, as from its design goals (\cref{sec:sandy}).
Since the configurations of bits 15 and 16 repeat over the memory pages as the colors in Nehalem, we assume negligible differences in the number of pages with respect to each bits configuration. Within these pages, the hash bits are evenly distributed, overall causing the page colors $ c_{SB} $ to be evenly distributed as well. Therefore, also with Sandy Bridge we can well approximate the percentage of memory to the percentage of \gls{llc}.

In general, this memory constraint can be a limitation for applications with a big memory working set and a small cache working set. Yet, applications with such characteristics are rare in practice, as the \gls{llc} footprint is roughly proportional to the memory occupation for compute-intensive applications. For other applications, a huge working set is often due to large \gls{i/o} buffers, which usually have low cache affinity and can be swapped out after use due to low reuse probability. To avoid situations where kernel \gls{i/o} buffers take most of the task's memory, we choose to allocate these buffers all over the \gls{ram} memory, without the restrictions of coloring. This also allows ensures to the kernel high availability of memory for its functioning, which is fundamental to preserve the machine responsiveness and stability. However, we believe that, in general, an approximate knowledge of the task's input on behalf of the user is sufficient to limit the cache-to-memory constraint: for example, some cloud infrastructures require end users to provide an a-priori estimation of the memory they use.


\subsection{Limits on physical page size} \label{sec:consequences_hugep}
The second limitation is the impossibility of using hugepages (see \cref{sec:hugepages}), available in some modern architectures.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/design/hugepages.pdf}
\caption{Bit fields for hugepages and \gls{llc}}
No bits are in common between hugepage address and \gls{llc} set index
\label{fig:hugepages}
\end{figure}

\Cref{fig:hugepages} shows the very different granularities of hugepages and of the \gls{llc} management that cause the color bits to overlap entirely with the page offset. Therefore, in Nehalem a single hugepage covers all the possible colors of the platform and is mapped to all the cache sets, denying isolation. Similarly, with Sandy Bridge the hugepage offset overlaps with bits 15 and 16 and with part of the page offset, used for the hash: the lack of control over these bits causes the data to be spread evenly among all the \gls{llc} slices, and in any set of the slice.

Since a hugepage causes data to be mapped to any set, neither isolated applications nor the others can leverage this feature. Unfortunately, the \gls{pipt} addressing makes this limitation impossible to relax. Page coloring and hugepages could coexist only with hardware changes like a more diverse granularity, but any such modofications are not planned in future \glspl{cmp}.

\section{Rainbow Buddy} \label{sec:buddy_mod}
Implementing page coloring requires noticeable modifications to the buddy algorithm and data structure. The aim of these modifications is to permit the allocation of a page of a specified color. These modifications are based on the \gls{llc} parameters identified in \cref{sec:cache_model}. In particular, \cref{sec:buddydata_mod} explains the modifications to the data structures and \cref{sec:buddyalg_mod} shows those to the basic algorithms for insertion and removal of buddies.


\subsection{Mcolors and data structure modifications} \label{sec:buddydata_mod}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/design/buddy_color.pdf}
\caption{Overlap of color bits and buddy bits in Nehalem}
\label{fig:buddycol}
\end{figure}

To allow ``colored'' allocations, the data structure of the buddy system must be modified to allow insertion and removal of pages of a specific color. The color is specified in the request by choosing one of the colors allocated to the application. We have to note that the notion of ``color'' applies natively to pages, that is to say, in the context of the buddy allocators, to buddies of order 0; and the color varies over consecutive pages. When the buddy order increases, more and more lower bits of the buddy address become 0 (see \cref{sec:buddydata}). Above a certain order the address bits being forced to 0 start overlapping with the color bits, as \cref{fig:buddycol} shows, and the configurations available for the color bits are less and less as the order increases. Thereof, after a certain order all the color bits are forced to 0. For Nehalem, all the colors are available if the zeroed bits are below bit 15, corresponding to orders 0 to 3 (included). Hence, for the buddies of these order the notion of color applies as well. Always considering Nehalem, buddies with order from 4 to 6 (included) have more and more color bits forced to 0, progressively reducing the possible color configurations from 8 to 2, and buddies with order higher than 6 have all the color bits being 0, thus one configuration. Considering, for example, a 4-order buddy, it spans two 3-order buddies which have two different colors, with the first 3 bits in common and the lowest bit variable. Hence, it is natural to identify the color of a 4-order buddy with the highest 3 bits only, as the fourth bit may vary. And, similarly, a 5-order buddy contains two 4-order buddies, and has only two fixed color bits; and so on. This leads us to define the notion of \emph{multi-color} or \emph{mcolor} in order to generalize the color of a buddy with respect to its order. Therefore, if $ b $ is the address of the buddy and $ d $ its order, we can mathematically define the mcolor for Nehalem as
\[
mcolor_N(b,d) = \left\{
	\begin{array}{ll}
		b_{18} b_{17} b_{16} b_{15} & \mbox{if}~ 0 \leq d \leq 3 \\
		b_{18} b_{17} b_{16} & \mbox{if}~ d = 4 \\
		b_{18} b_{17} & \mbox{if}~ d = 5 \\
		b_{18} & \mbox{if}~ d = 6 \\
		0 & \mbox{otherwise}
	\end{array}
\right.
\]

For Sandy Bridge, the presence of a hash function leads to troubles. In case of orders from 0 to 5 the definition of mcolor we have given still applies, simply replacing $ b_{18} b_{17} $ with $ h_1 h_2 $. Similarly, in case of order 7 or greater, our definition applies equally. The case of order 6 is particular because of the unpredictability of the hash function: in fact, a 6-order buddy with hash $ h_1 h_2 = 10 $ can, for example, be split in two 5-order buddies with hash $ h_1 h_2 = 10 $ and $ h_1 h_2 = 01 $ respectively, because bit 17 might be XORed with both $ h_1 $ and $ h_2 $, flipping both hash bits when it is 1 (as in the second buddy). As will be clear in the following, a definition of mcolor also for Sandy Bridge is still possible, but depends on the specific hash function. Therefore, we leave the details on this topic to \cref{chap:implem}.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/design/buddy_colored.pdf}
\caption{Rainbow Buddy data structure}
The designed data structure with colored lists per-order; in the higher levels multiple colors are aggregated
\label{fig:buddycolored}
\end{figure}

Leveraging the notion of mcolor, it is natural to split each order list into several sub-lists, one per mcolor, to keep the lookup operation fast and maintain efficiency. \Cref{fig:buddycolored} depicts the new structure \system buddy allocator uses: an array of lists per mcolor, which can be accessed in constant time by adding the mcolor to the base pointer.\\
Therefore, the final subdivision of buddy lists takes in account three aspects: order, migratetype (recalling \cref{sec:mobility}) and mcolor. In the following section, for the sake of simplicity we will ignore the presence of the migratetype; in fact, this parameter is constant during an allocation and does not vary often across allocations, since userspace requests (the only colored allocations) are typically fulfilled with buddies of the highest mobility.


\subsection{Algorithm modification} \label{sec:buddyalg_mod}
Leveraging the data structure we designed in the previous section, we consequently have to modify the buddy algorithm, with the goal of maintaining its efficiency.

Within the \system buddy allocator, the coalescing procedure is, overall, unchanged, since it depends on the physical contiguity of the two buddies. The only change is the final insertion, in which the insertion list now depends not only on the order and migratetype but also on the mcolor of the buddy.
The insertion algorithm is explained in the following pseudo-code snippet. Here, \code{MCOLOR} is the function that returns the buddy mcolor from \cref{sec:buddydata_mod}, either for Nehalem or for Sandy Bridge, and the buddy migratetype, being constant during the coalescing procedure, is not shown. The code snippet shows a recursive procedure that summarizes the insertion functioning in \system.

\begin{lstlisting}[style=pseudo]
globaldata: list_head buddies[MAX_ORDER][MAX_COLORS]

procedure INSERT_BUDDY
input:	buddy_address addr, buddy_order ord
output:	none
behavior:
	mcolor = MCOLOR(addr, ord)
	// address of the physically contiguous buddy
	twin_addr = addr + (1 XOR ord)	
	
	if (ord < MAX_ORDER - 1) AND (BUDDY_IS_FREE(twin_addr))
		new_buddy = COALESCE_BUDDIES(addr, twin_addr, ord)
		INSERT_BUDDY(new_buddy, ord + 1)
		return
	else
		INSERT_INTO_LIST_HEAD(buddies[ord][mcolor],addr)
end procedure
\end{lstlisting}

As from \cref{sec:buddy}, the ``twin'' physically contiguous buddy is computed by means of a XOR operation on the order-th bit of the buddy address. The procedures \code{BUDDY_IS_FREE} and \code{COALESCE_BUDDIES} act on the array of physical pages (described in \cref{sec:buddydata}) to store or retrieve information about the buddy (order, migratetype, allocation status, etc.), run in constant time and their details are of little interest here; however, the names explain their roles.

The splitting procedure, instead, is more complex. The key issue is that, if a buddy of a desired color is not present, the allocator must split a higher order buddy in which the requested color is present, to guarantee the allocation time be constant and avoid searching. To do this, we need a way to select the proper mcolor from the requested color. For the Nehalem architecture, we note that, increasing buddy order, the lower color bits are discarded in order to compute the mcolor. In a similar way, we can discard the lower bits of the requested color to have the mcolor of a certain order. For example, if a page of color 13 is requested and no buddy of order from 0 to 3 is present of color 13, the allocator has to split a buddy of higher order in which a page of color 13 is present. Color 13, or $ 1101_2 $ in binary format, corresponds to mcolor $ 110_2 $ of order 4: if a buddy of such order is present, it is split into two halves, differing in the lowest bit, thus being $ 1100_2 $ and $ 1101_2 $ respectively. After the split, the allocator continue the split with the second buddy to reach order 0 and store the former in the 3-order free list. Similarly, if no 4-order buddy of color $ 110_2 $ is present, the allocator has to split a 5-order buddy of color $ 11_2 $, choose the first half and split it further; and so on. This procedure, overall, is called \emph{color chasing} and is the key innovation of the splitting procedure. Leveraging mathematical formalism, if the requested color is $ c = c_3 c_2 c_1 c_0 $ and the requested buddy order being checked is $ d $, the mcolor to look for is:
\[
mcolor\_lookup_N(c,d) = \left\{
	\begin{array}{ll}
		c_3 c_2 c_1 c_0 & \mbox{if}~ 0 \leq d \leq 3 \\
		c_3 c_2 c_1 & \mbox{if}~ d = 4 \\
		c_3 c_2 & \mbox{if}~ d = 5 \\
		c_3 & \mbox{if}~ d = 6 \\
		0 & \mbox{otherwise}
	\end{array}
\right.
\]
Again, in Sandy Bridge this function depends on the hash, and we postpone the discussion to \cref{chap:implem}.

The following pseudo-code snippet summarizes the splitting procedure, which starts from \code{EXTRACT_PAGE}: this procedure looks for a page of the requested color. If it cannot find one, it invokes the procedure \code{SPLIT_BUDDY}, which looks for a suitable higher-order buddy performing color chasing by means of the functions $ mcolor $ and $ mcolor_lookup $ defined previously (in capital letters inside the code snippet).

\begin{lstlisting}[style=pseudo]
globaldata:	list_head buddies[MAX_ORDER][MAX_COLORS]

procedure SPLIT_BUDDY
input:	buddy_order ord, mcolor col
output: buddy_addr
behavior:
	buddy_order local_ord
	buddy_addr local_addr, twin_addr
	mcolor local_mcol, addr_mcol

	if ord == MAX_ORDER
		// no buddy exists
		return nil

	local_mcol = MCOLOR_LOOKUP(col, ord)
	if LIST_IS_EMPTY(buddies[ord][local_mcol])
		// no buddy found in this order
		local_addr = SPLIT_BUDDY(ord + 1, col)
		if local_addr == nil
			return nil
	else
		// get buddy for splitting
		local_addr = REMOVE_LIST_HEAD(buddies[ord][local_mcol])

	twin_addr = local_addr + (1 XOR ord)

	// color chasing
	addr_mcol = MCOLOR(local_addr, ord - 1)
	
	if first_mcol != local_mcol
		// twin_addr is the buddy to return
		SWAP_VARS(local_addr, twin_addr)
	INSERT_INTO_LIST_HEAD(buddies[ord - 1][addr_mcol],twin_addr)

	return local_addr
end procedure

procedure EXTRACT_PAGE
input:	mcolor mcol
output:	buddy_addr	
behavior:
	if LIST_IS_EMPTY(buddies[0][mcol])
		return SPLIT_BUDDY(0,mcol)
	else
		return REMOVE_LIST_HEAD(buddies[0][mcol])
end procedure

\end{lstlisting}

The procedure \code{SPLIT_BUDDY} starts by checking if buddies of the requested order may exist. Then, it looks for a suitable buddy to split, recursively requesting one if none is present in the current order. After finding the buddy to split, the procedure and computes the address of the ``twin'' buddy of smaller order. Then, it performs color chasing by checking which of the two buddies has the desired mcolor, either the local (variable \code{local_addr}) or the twin (variable \code{twin_addr}). In case the twin is the desired buddy, the procedure swaps the two variables so that, finally, \code{local_addr} contains the needed buddy and \code{twin_addr} the other one, to be stored in the free list.


\section{\system interface}
To exploit the \gls{llc} partitioning facilities introduced with \system, an application needs a suitable interface. In particular, an application should be able to declare the size of its \gls{llc} partition to the kernel via a userspace interface, so that the kernel, then, allocates its memory properly.
An additional system call is a technically viable solution to realize such interface, but is not in line with Linux development guidelines \footnote{Linux developers are unwilling to introduce new system calls, unless a wide audience of users shows an evident and frequent need of it.}. Moreover, since several parameters are of interest for the new \system capabilities, the system call interface can become complex and unclear.

In the present years, Linux developers prefer filesystem-like interfaces to expose non-core functionalities, like the generic \emph{sysfs} interface \cite{sysfs} and, in particular, the \emph{cgroups} \cite{cg} facility. A cgroup is, essentially, a hierarchical interface on a system resource (memory, \gls{i/o}, \glspl{cpu}, etc.) exposed as a folder hierarchy. A user can partition a resource by creating a subdirectory inside the main one. Each subdirectory contains several files, which control the resource parameters (for example, the memory amount, the \gls{i/o} bandwidth, the set of allowed cores, etc.). To change one of those parameters, it is sufficient to write the proper file, thus changing the resource allocation. Similarly, to constrain a process to a resource partition by associating the process to a cgroup, it is sufficient to write the \gls{pid} to a specific file.

Therefore, we chose to introduce a new cgroup called \emph{cacheset}, which serves as an interface to \system's capabilities. A user creates an \gls{llc} partition by making a subdirectory inside the main cacheset directory and chooses the number of colors of the partition by writing a file, and the tasks using that \gls{llc} partition by writing another.
This flow well fits the way distributed computing platforms are managed: typically, each server has a centralized manager that monitors running applications and creates new ones on demand. This manager can exploit the cacheset cgroup in the way described above, associating a task to a certain cgroup based on the task's memory footprint, the \gls{qos} requirements and any high-level policy.

On the kernel side, Linux automatically redirects the filesystem operations to the \gls{llc} management functions, in a transparent way to the user. Since recent Linux versions have a dedicated, independent interface to add a cgroup, no change is needed to the filesystem management. Thereof, adding cacheset consists essentially in ``hooking up'' into the Linux cgroup interface, as explained \cref{chap:implem}.
