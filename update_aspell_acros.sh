#!/bin/bash

if [[ -z "$1" ]]; then
    echo "please, provide the acronyms file"
    exit -1
fi

dictfile='acro_dict.txt'

acros=$(cat "$1" | grep '\\newacronym' | sed 's/\[.*\]//g' | cut -d '{' -f 2 | cut -d '}' -f 1 | aspell clean strict 2> /dev/null)

num=$(echo $acros | xargs -n1 | wc -l | tr -d ' ')

echo "personal_ws-1.1 en ${num}" > ${dictfile}
echo $acros | xargs -n1 >> ${dictfile}
