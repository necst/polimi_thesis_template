main := thesis

# all tex files in ALL subfolders (including .) without $(main).tex
texfiles = $(filter-out $(main).tex, $(shell find . -name "*.tex"))
# all text files with .tex extension replaced with .aux
auxfiles = $(patsubst %.tex,%.aux, $(other_texs))

acro_dict := ./acro_dict.txt
ifneq ($(wildcard $(acro_dict)),)
	dict_opt = --extra-dicts=$(acro_dict)
endif
ASPELL_OPT = --home-dir=. $(dict_opt)

LTC = pdflatex -interaction=nonstopmode -shell-escape -synctex=1
BIB = biber
GLO = makeglossaries

.PHONY: clean distclean aspell aspell_command

all: $(main).pdf

$(main).pdf: $(main).tex $(main).bib $(texfiles)
	$(LTC) $(main).tex
	$(BIB) $(main)
	$(GLO) $(main)
	$(LTC) $(main).tex
	$(GLO) $(main)
	$(LTC) $(main).tex

clean:
	@rm *.bak $(main).bcf $(main).idx $(main).run.xml $(main).aux $(auxfiles) $(main).bbl $(main).blg $(main).dvi $(main).log $(main).out $(main).pdf $(main).ps $(main).glo $(main).gls $(main).lot $(main).lof $(main).toc $(main).xdy $(main).glg $(main).synctex.gz &> /dev/null || true

distclean: clean
	@rm $(main).pdf &> /dev/null || true

aspell_command:
	@echo "check .aspell.conf for more info"
	@echo "aspell $(ASPELL_OPT) check <filename>"

aspell:
	@for f in $(texfiles) ; do \
		aspell $(ASPELL_OPT) check $$f; \
	done

