# PoliMi master thesis template

Based on the ``memoir`` class, this is a full featured example for the PoliMi thesis. The institution does not dictate specific style guidelines, and therefore there is a lot of freedom. To keep things tidy, the large amount of settings and dedicated macros was split in the two files ``style/style.tex`` and ``options_and_commands.tex``. Edit them to customize.

Requirements:

* pdflatex
* biber
* makeglossaries
* aspell (to spell-check)

To compile run ``make``, to clean intermediate files run ``make clean``and to clean everything ``make distclean``.

*TODO*: the graphic style of code snippets is not particularly appealing, you might want to change it and share with others within ``style/style.tex`` (styles can be named, so that multiple styles can be defined and chosen throughout the document).

## ASPELLing you text

For spell-checking the text, [aspell](http://aspell.net) is used. To spell-check **all** files in a row (except the main file ``thesis.tex``, which should contain only the structure), just run
```
make aspell
```
and aspell will prompt you all mis-spelled words, asking what to do for each one.

To get the command to spell a single file, you can run ``make aspell_command``.

Note that aspell is configured from local file ``.aspell.conf``, which contains additional options than the default:

* default language is English
* check mode is *tex*: TeX macros are ignored, as well as the arguments of the most common of them
* since the arguments to some TeX macros are not ignored, they are also added to ``.aspell.conf``, as in the case of ``\gls``and friends
* added words (i.e. those that you tell aspell are correct) are added to ``personal_dict.txt``, so that you can keep (and maybe git) this file for your usage, so that you don't check the same word twice

Refer to the comments in ``.aspell.conf`` for more information.

Finally, with the script ``update_aspell_acros.sh`` you can parse the list of ``\newacronym``s and generate a file ``acro_dict.txt`` with the acronyms to ignore:
```
./update_aspell_acros.sh <TeX file with acronyms, e.g. acronyms/acronyms.tex>
```
